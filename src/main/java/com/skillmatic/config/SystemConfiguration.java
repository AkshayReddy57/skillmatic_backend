package com.skillmatic.config;

public interface SystemConfiguration {
	
	public String getCategoryImageBaseUploadFolder();
	public String getProfileImageBaseUploadFolder();
	
	public String[] getAllowedDomains();
	
	public String getBaseURL();
	
	public String getResumeuUploadfolder();
}
