package com.skillmatic.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("prod")
public class ProdSystemConfiguration implements SystemConfiguration {
	private String campaignimageuploadfolder = 		"/var/www/html/img/campaign/";
	private String articleimagebaseuploadfolder =  	"/var/www/html/img/articles/";
	private String pictureimagebaseuploadfolder =  	"/var/www/html/img/pictures/";
	
	
	private String categoryimagebaseuploadfolder =  "/var/www/html/img/categories/";
	private String magazineimagebaseuploadfolder =  "/var/www/html/img/magazines/";
	
	private String profileimagebaseuploadfolder = "/var/www/html/skillmatic/img/profiles/";
	private String resumeuploadfolder = "/var/www/html/skillmatic/resume/profiles/";
	
	private String baseurl = "https://35.200.165.138";
	
	private String[] allowDomain = {"https://35.200.165.138","https://caerusskillmatic.com","http://35.200.165.138","http://caerusskillmatic.com", "https://35.229.48.151" , "http://35.229.48.151" , "http://localhost:4200"}; 
	
	public String[] getAllowedDomains() {
		return this.allowDomain;
	}
	
	
	public String getBaseURL() {
		return this.baseurl;
	}
	
	public String getCampaignImageBaseUploadFolder() {
		return this.campaignimageuploadfolder;
	}
	
	public String getArticleImageBaseUploadFolder() {
		return this.articleimagebaseuploadfolder;
	}

	public String getPictureImageBaseUploadFolder() {
		return this.pictureimagebaseuploadfolder;
	}

	public String getProfileImageBaseUploadFolder() {
		return this.profileimagebaseuploadfolder;
	}
	
	public String getCategoryImageBaseUploadFolder() {
		return this.categoryimagebaseuploadfolder;
	}
	public String getMagazineImageBaseUploadFolder() {
		return this.magazineimagebaseuploadfolder;
	}
	public String getResumeuUploadfolder() {
		return this.resumeuploadfolder;
	}
}
