package com.skillmatic.util;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

public class APIResponseObj {
	 
    private HttpStatus status;
    private String message;
    private String additionalinfo;
   // private Iterable iterable;
    private Object obj;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
 
    public APIResponseObj(String message, HttpStatus status, String additionalinfo, Object Obj){
        this.message = message;
        this.status = status;
        this.additionalinfo = additionalinfo;
        timestamp = LocalDateTime.now();
        this.obj = Obj;
    }
 
    
    public Object getObj() {
    		return obj;
    }
    
    public String getMessage() {
        return message;
    }
    
    public HttpStatus getStatus() {
		return status;
	}

    public String getAdditionalInfo() {
		return additionalinfo;
	}
    
    public LocalDateTime getTimestamp() {
		return timestamp;
	}
}