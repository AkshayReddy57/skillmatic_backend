package com.skillmatic.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.skillmatic.service.serviceimpl.UserRepositoryImpl;

@Service
@EnableAsync
public class MailClient {
	private static final Logger logger = LoggerFactory.getLogger(MailClient.class);
 
    private JavaMailSender mailSender;
 
    @Autowired
    public MailClient(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }
 
    //1. Login to Gmail. 
    //2. Access the URL as https://www.google.com/settings/security/lesssecureapps 
    //3. Select "Turn on"
    @Async
    public void prepareAndSend(String recipient, String message) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("caerusskillmatic@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Success - Temporary password request");
            messageHelper.setText(message);
        };
        try {
        	logger.debug("EMAIL......................." );
            mailSender.send(messagePreparator);
            logger.debug("EMAIL......................." );
        } catch (MailException e) {
            e.printStackTrace();
        }
    }
    
    
 
    
    @Async
    public void sendActivateRegistration(String recipient, String message) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("caerusskillmatic@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Caerus SkillmaTic platform Registration, Activation Mail");
            messageHelper.setText(message, true);
        };
        try {
        	logger.debug("EMAIL......................." );
            mailSender.send(messagePreparator);
            logger.debug("EMAIL......................." );
        } catch (MailException e) {
            e.printStackTrace();
        }
    }
    
    
    @Async
    public void sendResetPassword(String recipient, String message) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("caerusskillmatic@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Reset Password, Activation Mail");
            messageHelper.setText(message, true);
        };
        try {
        	logger.debug("EMAIL......................." );
            mailSender.send(messagePreparator);
            logger.debug("EMAIL......................." );
        } catch (MailException e) {
            e.printStackTrace();
        }
    }


}