package com.skillmatic.util.constants;

public enum UserStatus {
	
	Enable,
	Disable;
}
