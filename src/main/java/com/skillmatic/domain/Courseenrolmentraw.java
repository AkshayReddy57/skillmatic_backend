package com.skillmatic.domain;



import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



	@Entity
	@Table(name="courseenrolmentraw")
public class Courseenrolmentraw {

	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    @Column(name = "pkid", nullable = false, updatable = false)

	    private Long pkid;
	    
	    private Long courseid; 
	    private Long userid; 
	    private String role; 
	    private String name; 
	    private String total_time; 
	    private Long completion_percentage; 
	    private String expired_on_timestamp; 
	    private Date enrolled_on;
	    private Date expired_on;
		public Long getPkid() {
			return pkid;
		}
		public void setPkid(Long pkid) {
			this.pkid = pkid;
		}
		public Long getCourseid() {
			return courseid;
		}
		public void setCourseid(Long courseid) {
			this.courseid = courseid;
		}
		public Long getUserid() {
			return userid;
		}
		public void setUserid(Long userid) {
			this.userid = userid;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getTotal_time() {
			return total_time;
		}
		public void setTotal_time(String total_time) {
			this.total_time = total_time;
		}
		public Long getCompletion_percentage() {
			return completion_percentage;
		}
		public void setCompletion_percentage(Long completion_percentage) {
			this.completion_percentage = completion_percentage;
		}
		public String getExpired_on_timestamp() {
			return expired_on_timestamp;
		}
		public void setExpired_on_timestamp(String expired_on_timestamp) {
			this.expired_on_timestamp = expired_on_timestamp;
		}
		public Date getEnrolled_on() {
			return enrolled_on;
		}
		public void setEnrolled_on(Date enrolled_on) {
			this.enrolled_on = enrolled_on;
		}
		public Date getExpired_on() {
			return expired_on;
		}
		public void setExpired_on(Date expired_on) {
			this.expired_on = expired_on;
		}
	    
		
		
}