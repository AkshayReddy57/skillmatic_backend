package com.skillmatic.domain;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skillmatic.domain.security.Authority;
import com.skillmatic.domain.security.UserRole;

@Entity
@Table(name="user")
public class User  implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userId", nullable = false, updatable = false)
    private Long userId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    
    private String aadhar;
    private String pan;
    private String state;
    private String citizenship;
    private String address;
    
    private String smgoogle;
    private String smfacebook;
    private String smtwitter;
    private String smlinkedin;
    private String smyoutube;
    private String website;
    
    private String secondaryeducation;
    
    private String secedupercent;
    private String secedupassoutyr;
    private String puedu;
    private String puedupercent;
    private String puedupassoutyr;
    private String ugedu;
    private String ugspeciality;
    private String ugedupercent;
    private String ugedupassoutyr;
    private String pgedu;
    private String pgedupercent;
    private String pgedupassoutyr;
    
    private String experience;
    private String industry;
    private String subindustry;
    private String currentorg;
    private String designation;
    private String ctc;
    
    private String resumeurl;
    
    private String confirmtoken;
    

    private String nationality;
    
    private String keyskills;
    private Long lmsid;
    
    
    public Long getLmsId() {
        return lmsid;
    }

    public void setLmsId(Long lmsid) {
        this.lmsid =lmsid;
    }
	public String getKeyskills() {
		return keyskills;
	}

	public void setKeyskills(String keyskills) {
		this.keyskills = keyskills;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	private String profileimgurl;
    
    private String gender;
    private String dob;
    private String status;
    
    
    @Transient
    private String isadmin;
    
    @Transient
    private String centername;
    
    
    private String profiletype;
    
    public String getConfirmtoken() {
		return confirmtoken;
	}

	public void setConfirmtoken(String confirmtoken) {
		this.confirmtoken = confirmtoken;
	}

	public String getResumeurl() {
		return resumeurl;
	}

	public void setResumeurl(String resumeurl) {
		this.resumeurl = resumeurl;
	}

	public String getSecedupercent() {
		return secedupercent;
	}

	public void setSecedupercent(String secedupercent) {
		this.secedupercent = secedupercent;
	}

	public String getSecedupassoutyr() {
		return secedupassoutyr;
	}

	public void setSecedupassoutyr(String secedupassoutyr) {
		this.secedupassoutyr = secedupassoutyr;
	}

	public String getPuedu() {
		return puedu;
	}

	public void setPuedu(String puedu) {
		this.puedu = puedu;
	}

	public String getPuedupercent() {
		return puedupercent;
	}

	public void setPuedupercent(String puedupercent) {
		this.puedupercent = puedupercent;
	}

	public String getPuedupassoutyr() {
		return puedupassoutyr;
	}

	public void setPuedupassoutyr(String puedupassoutyr) {
		this.puedupassoutyr = puedupassoutyr;
	}

	public String getUgedu() {
		return ugedu;
	}

	public void setUgedu(String ugedu) {
		this.ugedu = ugedu;
	}

	public String getUgspeciality() {
		return ugspeciality;
	}

	public void setUgspeciality(String ugspeciality) {
		this.ugspeciality = ugspeciality;
	}

	public String getUgedupercent() {
		return ugedupercent;
	}

	public void setUgedupercent(String ugedupercent) {
		this.ugedupercent = ugedupercent;
	}

	public String getUgedupassoutyr() {
		return ugedupassoutyr;
	}

	public void setUgedupassoutyr(String ugedupassoutyr) {
		this.ugedupassoutyr = ugedupassoutyr;
	}

	public String getPgedu() {
		return pgedu;
	}

	public void setPgedu(String pgedu) {
		this.pgedu = pgedu;
	}

	public String getPgedupercent() {
		return pgedupercent;
	}

	public void setPgedupercent(String pgedupercent) {
		this.pgedupercent = pgedupercent;
	}

	public String getPgedupassoutyr() {
		return pgedupassoutyr;
	}

	public void setPgedupassoutyr(String pgedupassoutyr) {
		this.pgedupassoutyr = pgedupassoutyr;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getSubindustry() {
		return subindustry;
	}

	public void setSubindustry(String subindustry) {
		this.subindustry = subindustry;
	}

	public String getCurrentorg() {
		return currentorg;
	}

	public void setCurrentorg(String currentorg) {
		this.currentorg = currentorg;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getCtc() {
		return ctc;
	}

	public void setCtc(String ctc) {
		this.ctc = ctc;
	}

	public String getSecondaryeducation() {
		return secondaryeducation;
	}

	public void setSecondaryeducation(String secondaryeducation) {
		this.secondaryeducation = secondaryeducation;
	}

	public String getSmgoogle() {
		return smgoogle;
	}

	public void setSmgoogle(String smgoogle) {
		this.smgoogle = smgoogle;
	}

	public String getSmfacebook() {
		return smfacebook;
	}

	public void setSmfacebook(String smfacebook) {
		this.smfacebook = smfacebook;
	}

	public String getSmtwitter() {
		return smtwitter;
	}

	public void setSmtwitter(String smtwitter) {
		this.smtwitter = smtwitter;
	}

	public String getSmlinkedin() {
		return smlinkedin;
	}

	public void setSmlinkedin(String smlinkedin) {
		this.smlinkedin = smlinkedin;
	}

	public String getSmyoutube() {
		return smyoutube;
	}

	public void setSmyoutube(String smyoutube) {
		this.smyoutube = smyoutube;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}



	public String getAadhar() {
		return aadhar;
	}

	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

    public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}



	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}



	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


    @Column(name = "email")
    private String email;
   
    private String phone;

    private boolean enabled=true;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<UserRole> userRoles = new HashSet<>();

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    
    public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}


	public String getProfileimgurl() {
		return profileimgurl;
	}

	public void setProfileimgurl(String profileimgurl) {
		this.profileimgurl = profileimgurl;
	}


	private String source;
	
	private String jobrole;
	private String company;
	private String orgtype;
	private String orgsize;
	public void setOrgsize(String orgsize) {
		this.orgsize = orgsize;
	}

	private String city;
	private String country;
	private String universitysize;
	private String zipcode;
	private String position;
	private String institution;
	
	private String universityname;
	private String collegename;
	

    
    public String getUniversityname() {
		return universityname;
	}

	public void setUniversityname(String universityname) {
		this.universityname = universityname;
	}

	public String getCollegename() {
		return collegename;
	}

	public void setCollegename(String collegename) {
		this.collegename = collegename;
	}

 
   
	public String getJobrole() {
		return jobrole;
	}

	public void setJobrole(String jobrole) {
		this.jobrole = jobrole;
	}



	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + ", password=" + password + ", firstName="
				+ firstName + ", lastName=" + lastName + ", aadhar=" + aadhar + ", pan=" + pan + ", state=" + state
				+ ", citizenship=" + citizenship + ", address=" + address + ", smgoogle=" + smgoogle + ", smfacebook="
				+ smfacebook + ", smtwitter=" + smtwitter + ", smlinkedin=" + smlinkedin + ", smyoutube=" + smyoutube
				+ ", website=" + website + ", secondaryeducation=" + secondaryeducation + ", secedupercent="
				+ secedupercent + ", secedupassoutyr=" + secedupassoutyr + ", puedu=" + puedu + ", puedupercent="
				+ puedupercent + ", puedupassoutyr=" + puedupassoutyr + ", ugedu=" + ugedu + ", ugspeciality="
				+ ugspeciality + ", ugedupercent=" + ugedupercent + ", ugedupassoutyr=" + ugedupassoutyr + ", pgedu="
				+ pgedu + ", pgedupercent=" + pgedupercent + ", pgedupassoutyr=" + pgedupassoutyr + ", experience="
				+ experience + ", industry=" + industry + ", subindustry=" + subindustry + ", currentorg=" + currentorg
				+ ", designation=" + designation + ", ctc=" + ctc + ", resumeurl=" + resumeurl + ", confirmtoken="
				+ confirmtoken + ", nationality=" + nationality + ", profileimgurl=" + profileimgurl + ", gender="
				+ gender + ", dob=" + dob + ", status=" + status + ", isadmin=" + isadmin + ", centername=" + centername
				+ ", profiletype=" + profiletype + ", email=" + email + ", phone=" + phone + ", enabled=" + enabled
				+ ", userRoles=" + userRoles + ", source=" + source + ", jobrole=" + jobrole + ", company=" + company
				+ ", orgtype=" + orgtype + ", orgsize=" + orgsize + ", city=" + city + ", country=" + country
				+ ", universitysize=" + universitysize + ", zipcode=" + zipcode + ", position=" + position
				+ ", institution=" + institution + ", universityname=" + universityname + ", collegename=" + collegename
				+ "]";
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getOrgtype() {
		return orgtype;
	}

	public void setOrgtype(String orgtype) {
		this.orgtype = orgtype;
	}


	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}



	public String getUniversitysize() {
		return universitysize;
	}

	public void setUniversitysize(String universitysize) {
		this.universitysize = universitysize;
	}

	

	public String getPosition() {
		return position;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getOrgsize() {
		return orgsize;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getProfiletype() {
		return profiletype;
	}

	public void setProfiletype(String profiletype) {
		this.profiletype = profiletype;
	}

	public String getCentername() {
		return centername;
	}

	public void setCentername(String centername) {
		this.centername = centername;
	}

	//@JsonIgnore
	public String getIsadmin() {
		return isadmin;
	}

	public void setIsadmin(String isadmin) {
		this.isadmin = isadmin;
	}

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<>();
        userRoles.forEach(ur -> authorities.add(new Authority(ur.getRole().getName())));
        return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

    

 


}
