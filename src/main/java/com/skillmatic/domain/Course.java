package com.skillmatic.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="course")
public class Course {

	 @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    @Column(name = "pkid", nullable = false, updatable = false)
	 	private Long pkid;

	 	private Long id;
	    private String name;
	    private String code;
	    private String category_id;
	    private String description;
	    private String price;
		private String status;
	    private Date creation_date;
	    private Date last_update_on;
	    private Long creator_id;
	    private String hide_from_catalog;
		private String time_limit;
	    private String level;
	    private String shared;
	    private String shared_url;
	    private String avatar;
	    private String big_avatar;
	    private String certification;
	    private String certification_duration;
		public Long getPkid() {
			return pkid;
		}
		public void setPkid(Long pkid) {
			this.pkid = pkid;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getCategory_id() {
			return category_id;
		}
		public void setCategory_id(String category_id) {
			this.category_id = category_id;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getPrice() {
			return price;
		}
		public void setPrice(String price) {
			this.price = price;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public Date getCreation_date() {
			return creation_date;
		}
		public void setCreation_date(Date creation_date) {
			this.creation_date = creation_date;
		}
		public Date getLast_update_on() {
			return last_update_on;
		}
		public void setLast_update_on(Date last_update_on) {
			this.last_update_on = last_update_on;
		}
		public Long getCreator_id() {
			return creator_id;
		}
		public void setCreator_id(Long creator_id) {
			this.creator_id = creator_id;
		}
		public String getHide_from_catalog() {
			return hide_from_catalog;
		}
		public void setHide_from_catalog(String hide_from_catalog) {
			this.hide_from_catalog = hide_from_catalog;
		}
		public String getTime_limit() {
			return time_limit;
		}
		public void setTime_limit(String time_limit) {
			this.time_limit = time_limit;
		}
		public String getLevel() {
			return level;
		}
		public void setLevel(String level) {
			this.level = level;
		}
		public String getShared() {
			return shared;
		}
		public void setShared(String shared) {
			this.shared = shared;
		}
		public String getShared_url() {
			return shared_url;
		}
		public void setShared_url(String shared_url) {
			this.shared_url = shared_url;
		}
		public String getAvatar() {
			return avatar;
		}
		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}
		public String getBig_avatar() {
			return big_avatar;
		}
		public void setBig_avatar(String big_avatar) {
			this.big_avatar = big_avatar;
		}
		public String getCertification() {
			return certification;
		}
		public void setCertification(String certification) {
			this.certification = certification;
		}
		public String getCertification_duration() {
			return certification_duration;
		}
		public void setCertification_duration(String certification_duration) {
			this.certification_duration = certification_duration;
		}
	    

	 	
}