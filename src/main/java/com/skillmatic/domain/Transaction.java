package com.skillmatic.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transaction")

public class Transaction {

	@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
		private Integer transactionid;
		private String orderid; 
		private String courseid;
		private String userid;
		private String price;
		private String payment_mode;
		private String status;
		private String time_stamp;
		
		

		public Integer getTransactionid() {
			return transactionid;
		}
		public void setTransactionid(Integer transactionid) {
			this.transactionid = transactionid;
		}
		public String getOrderid() {
			return orderid;
		}
		public void setOrderid(String orderid) {
			this.orderid = orderid;
		}
		public String getCourseid() {
			return courseid;
		}
		public void setCourseid(String courseid) {
			this.courseid = courseid;
		}
		public String getuserid() {
			return userid;
		}
		public void setUserid(String userid) {
			this.userid = userid;
		}
		public String getPrice() {
			return price;
		}
		public void setPrice(String price) {
			this.price = price;
		}
		public String getPayment_mode() {
			return payment_mode;
		}
		public void setPayment_mode(String payment_mode) {
			this.payment_mode = payment_mode;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getTime_stamp() {
			return time_stamp;
		}
		public void setTime_stamp(String time_stamp) {
			this.time_stamp = time_stamp;
		}
			
	
}