package com.skillmatic.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="category")

public class Category {

	@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    @Column(name = "pkid", nullable = false, updatable = false)
	 	private Long pkid;
		private Long id;
		private String name; 
		private String price;
		private String parent_category_id;
		
		public Long getPkid() {
			return pkid;
		}
		public void setPkid(Long pkid) {
			this.pkid = pkid;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getPrice() {
			return price;
		}
		public void setPrice(String price) {
			this.price = price;
		}
		public String getParent_category_id() {
			return parent_category_id;
		}
		public void setParent_category_id(String parent_category_id) {
			this.parent_category_id = parent_category_id;
		}
			
	
}