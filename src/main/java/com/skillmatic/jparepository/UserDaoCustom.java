package com.skillmatic.jparepository;

import java.util.List;

import com.skillmatic.domain.User;

public interface UserDaoCustom {

	User findUsersByQuery();
	User getUser(String name);
	
	
	int updateProfileImgUrl(String profileimgurl, Long userid);
}
