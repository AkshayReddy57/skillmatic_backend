package com.skillmatic.jparepository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skillmatic.domain.CourseEnrolment;
@Repository
public interface CourseEnrolmentDao extends CrudRepository<CourseEnrolment, Long> {

	List<CourseEnrolment> findAll();
	List<CourseEnrolment> findByUserid(Long userId);
    
}
