package com.skillmatic.jparepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skillmatic.domain.Institution;
@Repository
public interface InstitutionDao extends CrudRepository<Institution, Long> {

	
}
