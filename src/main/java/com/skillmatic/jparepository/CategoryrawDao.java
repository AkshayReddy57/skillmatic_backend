package com.skillmatic.jparepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skillmatic.domain.Categoryraw;

@Repository
public interface CategoryrawDao extends CrudRepository<Categoryraw, Long>{

}
