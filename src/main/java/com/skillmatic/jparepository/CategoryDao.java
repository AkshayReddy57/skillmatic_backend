
package com.skillmatic.jparepository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skillmatic.domain.Category;

@Repository
public interface CategoryDao extends CrudRepository<Category, Long>{
	// List<Category> findAll();
}
