package com.skillmatic.jparepository;

import org.springframework.data.repository.CrudRepository;

import com.skillmatic.domain.security.Role;

public interface RoleDao extends CrudRepository<Role, Integer> {
    Role findByName(String name);
}
