package com.skillmatic.jparepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.skillmatic.domain.Courseenrolmentraw;

@Repository
public interface CourseenrolmentrawDao extends CrudRepository<Courseenrolmentraw, Long> {

}
