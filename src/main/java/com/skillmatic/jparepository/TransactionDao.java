
package com.skillmatic.jparepository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skillmatic.domain.Transaction;

@Repository
public interface TransactionDao extends CrudRepository<Transaction, Long>{
	// List<Category> findAll();
}
