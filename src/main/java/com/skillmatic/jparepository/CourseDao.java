package com.skillmatic.jparepository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skillmatic.domain.Course;

@Repository
public interface CourseDao extends CrudRepository<Course, Long> {

    List<Course> findAll();
    
	
}