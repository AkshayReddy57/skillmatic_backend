package com.skillmatic.service;

import java.util.List;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.skillmatic.domain.Transaction;
import com.skillmatic.domain.User;

public interface TransactionService {

	 void save (Transaction transaction);
	 Transaction createTransaction(Transaction transaction);
	    
	
}

