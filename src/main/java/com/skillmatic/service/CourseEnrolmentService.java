package com.skillmatic.service;

import com.skillmatic.domain.CourseEnrolment;

public interface CourseEnrolmentService {
	
	public Iterable<CourseEnrolment> findCourseEnrolmentList() ;
	public Iterable<CourseEnrolment> findCourseEnrolmentByUserid(Long userId);
}
