package com.skillmatic.service;

import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.skillmatic.domain.Category;

public interface CategoryService {

	public Iterable<Category> findCategoryList() ;

	public Iterable<JSONObject> getListofCategoryMenu();
	
}

