package com.skillmatic.service;

import java.util.List;

import com.skillmatic.domain.Category;
import com.skillmatic.domain.Course;
import com.skillmatic.domain.User;

public interface CourseService {

	public Iterable<Course> findCourseList() ;
	
	
//	public List<User> getListofTrainersByCenter(String centerid);
	
	 public List<Course> getListofCoursesByCategory(String category);
	 public List<Course> getListofCoursesByCategoryID(Long id);
	 public List<Course> getCourseByCourseid(Long id);


}
