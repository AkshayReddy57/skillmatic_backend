package com.skillmatic.service;

import com.skillmatic.domain.Institution;

public interface InstitutionService {
	
	 public Iterable<Institution> findInstitutionsList() ;

}
