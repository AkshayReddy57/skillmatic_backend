package com.skillmatic.service.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
// import javax.persistence.Query;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skillmatic.domain.Category;
import com.skillmatic.domain.Course;
import com.skillmatic.domain.User;
import com.skillmatic.jparepository.CourseDao;
import com.skillmatic.service.CourseService;

//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.Repository;
//import org.springframework.data.repository.query.Param;

@Service
@Transactional
public class CourseRepositoryImpl implements CourseService  {

	private static final Logger LOG = LoggerFactory.getLogger(UserRepositoryImpl.class);
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Autowired
	private CourseDao courseDao;
	

	 public Iterable<Course> findCourseList() {
		 
		return  courseDao.findAll();
		 
	        
	    }

	 public List<Course> getListofCoursesByKeyword(String keyword) {
		  
		
			Query query = entityManager.createNativeQuery("select * from course "
					+ "	where category_id in (  with recursive cte (id, name, parent_category_id) as ( select     id, " + 
					"name, parent_category_id from category where upper(name) like upper('%"+keyword +"%') union all select p.id, " + 
					"p.name, p.parent_category_id from category p inner join cte on p.parent_category_id = cte.id ) " + 
					"select id from cte ) or  upper(name) like upper('%" + keyword +"%') " + 
					 "or  upper(description) like upper ('%"+keyword+"%')",  Course.class);
			
				return query.getResultList();

		}

	
	public List<Course> getListofCoursesByCategory(String category) {
		
		Query query = entityManager.createNativeQuery("select * from course "
				+ "	where category_id in (  with recursive cte (id, name, parent_category_id) as ( select     id, " + 
				"name, parent_category_id from category where upper(name) like upper('%"+category +"%') union all select p.id, " + 
				"p.name, p.parent_category_id from category p inner join cte on p.parent_category_id = cte.id ) " + 
				"select id from cte )",  Course.class);
		return query.getResultList();
	}

	 public List<Course> getListofCoursesByCategoryID(Long id) {
		  
			
			
			Query query = entityManager.createNativeQuery("select * from course "
					+ "	where category_id in (  with recursive cte (id, name, parent_category_id) as ( select     id, " + 
					"name, parent_category_id from category where id = "+id+" union all select p.id, " + 
					"p.name, p.parent_category_id from category p inner join cte on p.parent_category_id = cte.id ) " + 
					"select id from cte ) ",  Course.class);
			
				return query.getResultList();

		}

	public List<Course> getCourseByCourseid(Long id) {
		Query query = entityManager.createNativeQuery("select * from course where id = ?",  Course.class); 
		query.setParameter(1, id);
		return (List<Course>) query.getResultList();
		
		
	}

	 
}

