package com.skillmatic.service.serviceimpl;


import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.skillmatic.service.StartupLoadersService;

@Component
public class StartupLoadersServiceImpl implements StartupLoadersService {
	private static final Logger logger = LoggerFactory.getLogger(StartupLoadersServiceImpl.class);
	
 
    @PostConstruct
    @Transactional
    public void startupLoadAll(){
    	try {
    	    logger.debug("Init In Progress...");
    	} catch (Exception e) {
    		logger.debug("Error in Method >> startupLoadAll >> " + e.getMessage());
    	}
    }

}
