package com.skillmatic.service.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import com.skillmatic.domain.User;
import com.skillmatic.domain.security.UserRole;
import com.skillmatic.jparepository.RoleDao;
import com.skillmatic.jparepository.UserDao;
import com.skillmatic.jparepository.UserDaoCustom;
import com.skillmatic.service.UserService;
import com.skillmatic.util.constants.UserStatus;

@Service
@Transactional
public class UserRepositoryImpl implements UserService, UserDaoCustom {
	
	private static final Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Autowired
	private UserDao userDao; 
	
	@Autowired
    private RoleDao roleDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
//    @Autowired
//    private AccountService accountService;
	
	public void save(User user) {
        userDao.save(user);
    }

    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }
    
    
    public User createUser(User user, Set<UserRole> userRoles) {
        User localUser = userDao.findByUsername(user.getUsername());
        
        if (localUser != null) {
            logger.info("User with username {} already exist. Nothing will be done. ", user.getUsername());
        } else {
            String encryptedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(encryptedPassword);

            for (UserRole ur : userRoles) {
                roleDao.save(ur.getRole());
            }

            user.getUserRoles().addAll(userRoles);

            localUser = userDao.save(user);

            
        }

        return localUser;
    }
    
    
    public int updateUser(User user) {
    	System.out.println("profile update "+ user.getUserId());
    	
    	
    	Query query = entityManager.createNativeQuery("update user set "
    			+ " firstname = ? ," 
    			+ " lastname = ? ," 
    			+ " email = ? ," 
    			+ " phone = ? ," 
    			+ " dob = ? ," 
    			+ " gender = ? ," 
    			+ " country = ? ," 
    			+ " citizenship = ? ," 
    			+ " city = ? ," 
    			+ " state = ? ," 
    			+ " aadhar = ? ,"
    			+ " pan = ? ," 
    			+ " website = ? ," 
				+ " smgoogle = ? ," 
				+ " smfacebook = ? ," 
				+ " smtwitter = ? ," 
				+ " smlinkedin = ? ," 
				+ " smyoutube = ? ," 
				+ " secondaryeducation = ? ,"
    			+ " secedupercent = ? ,"
    			+ " secedupassoutyr = ? ,"
    			+ " nationality = ? "
    			
    			
    			+ "  where userid  = ? ");
    	
		query.setParameter(1, user.getFirstName());
		query.setParameter(2, user.getLastName());
		query.setParameter(3, user.getEmail());
		query.setParameter(4, user.getPhone());
		query.setParameter(5, user.getDob());
		query.setParameter(6, user.getGender());
		query.setParameter(7, user.getCountry());
		query.setParameter(8, user.getCitizenship());
		query.setParameter(9, user.getCity());
		query.setParameter(10, user.getState());
		query.setParameter(11, user.getAadhar());
		query.setParameter(12, user.getPan());
		query.setParameter(13, user.getWebsite());
		query.setParameter(14, user.getSmgoogle());
		query.setParameter(15, user.getSmfacebook());
		query.setParameter(16, user.getSmtwitter());
		query.setParameter(17, user.getSmlinkedin());
		query.setParameter(18, user.getSmyoutube());
		
		query.setParameter(19, user.getNationality());
		
		query.setParameter(20, user.getUserId());
		
		int rowcount = query.executeUpdate();
		
		return rowcount;
    	
    	
    
	}
    
    public int updateUserDetails(User user) {
    	System.out.println("profile update "+ user.getUserId());
    	
    	Query query = entityManager.createNativeQuery("update user set "
    			+ " firstname = ? ," 
    			+ " lastname = ? ," 
    			+ " phone = ? ," 
    			+ " gender = ? ," 
    			+ " country = ? ," 
    			+ " city = ? ," 
    			+ " state = ? ," 
    			+ " aadhar = ? ," 
    			+ " pan = ? ," 
    		
    			+ " secondaryeducation = ? ," 
				+ " secedupercent = ? ,"
    			+ " secedupassoutyr = ? ,"
				
    			+ " puedu = ? ,"
    			+ " puedupercent = ? ,"
				+ " puedupassoutyr = ? ,"
				
				+ " ugedu = ? ,"
				+ " ugspeciality = ? ,"
				+ " ugedupercent = ? ,"
				+ " ugedupassoutyr = ? ,"
				
				+ " pgedu = ? ,"
				+ " pgedupercent = ? ,"
				+ " pgedupassoutyr = ? ,"
				+ " experience = ? ,"
				+ " industry = ? ,"
				
				+ " subindustry = ? ,"
				+ " currentorg = ? ,"
				+ " designation = ? ,"


    			+ " website = ? ," 
				+ " smgoogle = ? ," 
				+ " smfacebook = ? ," 
				+ " smtwitter = ? ," 
				+ " smlinkedin = ? ," 
				+ " smyoutube = ? " 
				
    			+ " where userid  = ? ");
    	

		query.setParameter(1, user.getFirstName());
		query.setParameter(2, user.getLastName());
		query.setParameter(3, user.getPhone());
		query.setParameter(4, user.getGender());
		query.setParameter(5, user.getCountry());
		query.setParameter(6, user.getCity());
		query.setParameter(7, user.getState());
		query.setParameter(8, user.getAadhar());
		query.setParameter(9, user.getPan());
		
		query.setParameter(10, user.getSecondaryeducation());
		query.setParameter(11, user.getSecedupercent());
		query.setParameter(12, user.getSecedupassoutyr());
		
		query.setParameter(13, user.getPuedu());
		query.setParameter(14, user.getPuedupercent());
		query.setParameter(15, user.getPuedupassoutyr());
		
		query.setParameter(16, user.getUgedu());
		query.setParameter(17, user.getUgspeciality());
		query.setParameter(18, user.getUgedupercent());
		query.setParameter(19, user.getUgedupassoutyr());
		
		query.setParameter(20, user.getPgedu());
		query.setParameter(21, user.getPgedupercent());
		query.setParameter(22, user.getPgedupassoutyr());
		query.setParameter(23, user.getExperience());
		query.setParameter(24, user.getIndustry());
		query.setParameter(25, user.getSubindustry());
		query.setParameter(26, user.getCurrentorg());
		query.setParameter(27, user.getDesignation());


		query.setParameter(28, user.getWebsite());
		query.setParameter(29, user.getSmgoogle());
		query.setParameter(30, user.getSmfacebook());
		query.setParameter(31, user.getSmtwitter());
		query.setParameter(32, user.getSmlinkedin());
		query.setParameter(33, user.getSmyoutube());
		
		
		query.setParameter(34, user.getUserId());
		
		int rowcount = query.executeUpdate();
		
		return rowcount;
    	
    	
    
	}
    
    public User createGPUser(User user,  Set<UserRole> userRoles) {
    	 	for (UserRole ur : userRoles) {
             roleDao.save(ur.getRole());
         }
    	 	 user.getUserRoles().addAll(userRoles);
    	 	 User localUser = userDao.save(user);
    	 	 return localUser;
    }
    
    public User createFBUser(User user,  Set<UserRole> userRoles) {
	 	for (UserRole ur : userRoles) {
         roleDao.save(ur.getRole());
     }
	 	 user.getUserRoles().addAll(userRoles);
	 	 User localUser = userDao.save(user);
	 	 return localUser;
}
    
    public User createClientTrainer(User user,  Set<UserRole> userRoles) {
	    	for (UserRole ur : userRoles) {
            roleDao.save(ur.getRole());
        }
   	 	 user.getUserRoles().addAll(userRoles);
   	 	 User localUser = userDao.save(user);
   	 	 return localUser;
    }
    
    public List<User> getListofTrainersByCenter(String centerid) {
  
		
		Query query = entityManager.createNativeQuery("select * from user a , user_role b where a.centerid = ? and \r\n" + 
				"b.user_id = a.userid and \r\n" + 
				"b.`role_id` = 3", User.class);
		query.setParameter(1, centerid);
		return query.getResultList();
		
	}
    
    public int activateUser(String confirmtokenid) {
		User user = checkConfirmTokenExists(confirmtokenid);
		int rowcount = 0;
		
		if(user != null) {
			Query query = entityManager.createNativeQuery("update user set enabled = ?  where userid  = ? ");
			
			query.setParameter(1, 1);
			query.setParameter(2, user.getUserId());
			
			rowcount = query.executeUpdate();
		} 
		return rowcount;
    }

    
    public int resetpassword(String confirmtokenid, String newpassword) throws Exception{
		
		Query query = entityManager.createNativeQuery("update user set password = ?  where userid  = ? ");
		
		User user = checkConfirmTokenExists(confirmtokenid);
		
		System.out.println("in reset password userid >> " + user.getUserId());
		System.out.println("in reset password pass >> " + passwordEncoder.encode(newpassword));
		
		query.setParameter(1, passwordEncoder.encode(newpassword));
		query.setParameter(2, user.getUserId());
		
		return query.executeUpdate();
	
    }
    
    public int updateConfirmToken(Long userid, String confirmtokenid) {
    	Query query = entityManager.createNativeQuery("update user set confirmtoken = ?  where userid  = ? ");
		
		
		
		query.setParameter(1, confirmtokenid);
		query.setParameter(2, userid);
		
		return query.executeUpdate();
    }
    
    public int resettemppassword(Long userId, String newpassword) throws Exception{
    		String encryptedPassword = passwordEncoder.encode(newpassword);
    		Query query = entityManager.createNativeQuery("update user set password = ?  where userid  = ? ");
		
		query.setParameter(1, passwordEncoder.encode(newpassword));
		query.setParameter(2, userId);
		
		return query.executeUpdate();
    	
    }
    
//    @input username can be a usename or email depending on frontend user choice
    public boolean checkUserExists(String username){
        if (checkUsernameExists(username) || checkEmailExists(username)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean checkUserExists(String username, String email){
        if (checkUsernameExists(username) || checkEmailExists(email)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkUsernameExists(String username) {
        if (null != findByUsername(username)) {
            return true;
        }

        return false;
    }
    
    public boolean checkEmailExists(String email) {
        if (null != findByEmail(email)) {
            return true;
        }

        return false;
    }
    
    
    public User checkConfirmTokenExists(String confirmToken) {
    	User user = null;
    	
    	Query query = entityManager.createNativeQuery("select * from user where confirmtoken = ?  ", User.class);
		
		query.setParameter(1, confirmToken);
		
		
		List rowcount = query.getResultList();
		
		System.out.println("KKKK >><< "+ rowcount.size());
		
		if(rowcount.size() > 0) {
			user =  (User)query.getSingleResult();
			
		} 
		return user;
    }
    
    
    public boolean checkProfileEmailExists(String email) {
    	Query query = entityManager.createNativeQuery("select * from user where email = ?  and enabled = '1' ");
		
		query.setParameter(1, email);
		
		List rowcount = query.getResultList();
		
		if(rowcount.size() > 0) {
			return true;
		} else {
			return false;
		}
    }
    
    public boolean checkFBSocialIdExists(String socialid) {
    		Query query = entityManager.createNativeQuery("select * from user where socialid = ? and source = 'FB' ");
		
    		query.setParameter(1, socialid);
		
    		List rowcount = query.getResultList();
    		
    		if(rowcount.size() > 0) {
    			return true;
    		} else {
    			return false;
    		}
    	
    }
    
    
    public void refreshCourses() {
    	
    	
    	
//    	Query query = entityManager.createNamedStoredProcedureQuery("batchdatamove");
//    	System.out.println("call succes 111" );
//    	int i = query.executeUpdate();
//    	
    //	System.out.println("call succes" + i);
    	
    }
    
    public boolean checkPhoneforCenterExist(String phone, String centerid) {
    		Query query = entityManager.createNativeQuery("select * from user where phone = ? and centerid = ? ");
		
		query.setParameter(1, phone);
		query.setParameter(2, centerid);
	
		List rowcount = query.getResultList();
		
		if(rowcount.size() > 0) {
			return true;
		} else {
			return false;
		}
    }
    
    
    
    public boolean checkGPSocialIdExists(String socialid) {
    		Query query = entityManager.createNativeQuery("select * from user where socialid = ? and source = 'GP' ");
		query.setParameter(1, socialid);
		
		List rowcount = query.getResultList();
		
		if(rowcount.size() > 0) {
			return true;
		} else {
			return false;
		}
    	
    }

    public User saveUser (User user) {
        return userDao.save(user);
    }
    
    public List<User> findUserList() {
        return userDao.findAll();
    }

    public void enableUser (String username) {
        User user = findByUsername(username);
        user.setEnabled(true);
        userDao.save(user);
    }

    public void disableUser (String username) {
        User user = findByUsername(username);
        user.setEnabled(false);
        //logger.debug(user.isEnabled());
        userDao.save(user);
        logger.debug(username + " is disabled.");
    }
    
    public  User  findUsersByQuery() {
    	
		 Query query = entityManager.createNativeQuery("SELECT * FROM user where 1 =1 limit 1", User.class);
	
	        return (User)query.getSingleResult();
	}
    
    
    public User findUserByIdAndCenter(Long userId, String centerId) {
    		User user = null;
		try {
			logger.debug("AAAAAAAA" + userId + ">>>>>"+ centerId);
		Query query = entityManager.createNativeQuery("SELECT * FROM user u where  u.userid = ? and u.centerid = ? ", User.class);
		 
		query.setParameter(1, userId);
		query.setParameter(2, centerId);

		user = (User)query.getSingleResult();
		} catch (Exception e) {
			logger.debug("Error while fetching user/center values "+e.getMessage());
			return user; 
		}
		 return user; 
    }
    
    public int updatePassword(String newpassword, String userid) {
		Query query = entityManager.createNativeQuery("update user set password = ?  where userid  = ? ");
		
		String encryptedPassword = passwordEncoder.encode(newpassword);
		System.out.println("enc pass >>  " + encryptedPassword);
		query.setParameter(1, encryptedPassword);
		query.setParameter(2, userid);
		
		int rowcount = query.executeUpdate();
		System.out.println("row coun" + rowcount);
		
		return rowcount;
				
	}
    
	public int updateProfileImgUrl(String profileimgurl, Long userid) {
		Query query = entityManager.createNativeQuery("update user set profileimgurl = ?  where userid  = ? ");
		query.setParameter(1, profileimgurl);
		query.setParameter(2, userid);
		
		int rowcount = query.executeUpdate();
		
		return rowcount;
				
	}

	public int updateResumeUrl(String resumeurl, Long userid) {
		Query query = entityManager.createNativeQuery("update user set resumeurl = ?  where userid  = ? ");
		query.setParameter(1, resumeurl);
		query.setParameter(2, userid);
		
		int rowcount = query.executeUpdate();
		
		return rowcount;
				
	}
    
    public void deleteUserById(Long userId) {
    	userDao.deleteByUserId(userId);
    }

	@Override
	public User findUserById(Long userId) {
		
		return userDao.findByUserId(userId);
	}

	@Override
	public List<User> findByEnabledUser(UserStatus enable) {
		
		return userDao.findByEnabled(true);
	}

	@Override
	public List<User> findByDisabledUser(UserStatus disable) {
	
		return userDao.findByEnabled(false);
	}
	
	@SuppressWarnings("unchecked")
    public User getUser(String name){
		logger.debug("getUser:" + name);
		User user = null;
		
		Query query = entityManager.createNativeQuery("SELECT * FROM user where   username = ? or email = ? ", User.class);
		 
		query.setParameter(1, name);
		query.setParameter(2, name);

		user = (User)query.getSingleResult();

		 return user; 
	}
	
	
	
	public User getUserByEmail(String emailid) {
		
		User user = null;
		
		Query query = entityManager.createNativeQuery("SELECT * FROM user where   email = ?  ", User.class);
		 
		query.setParameter(1, emailid);
		

		user = (User)query.getSingleResult();

		 return user; 
	}
	
	
	
	
	@SuppressWarnings("unchecked")
    public String getCentername(String centerid){
		//logger.debug("getUser:" + name);
		String centername = null;
		
		Query query = entityManager.createNativeQuery("SELECT name FROM centers where  id = ? ");
		 
		query.setParameter(1, centerid);
		
		centername = (String)query.getSingleResult();

		 return centername; 
	}
	
	//&&&
	
//	@SuppressWarnings("unchecked")
//    public User getUserObj(String name){
//		
//	
//	
//		Query query = entityManager.createNativeQuery(" SELECT u.userid, u.username, u.password, u.firstname, u.lastname, u.email, u.phone, u.centerid, c.name "
//				
//				+ " FROM user u, centers c where c.id = u.centerid and u.username = ? or u.email = ?  ");
//		
//		query.setParameter(1, name);
//		query.setParameter(2, name);
//	
//			List<Object[]> queryRes = query.getResultList();
//			User user = null;
//			
//			for (Object[] row : queryRes) {
//			
//				user = new User();
//				
//				user.setUserId(((java.math.BigInteger) row[0]).longValue());
//				user.setUsername((String) row[1]);
//				
//				user.setPassword((String) row[2]);
//				
//				user.setFirstName((String) row[3]);
//				user.setLastName((String) row[4]);
//				user.setEmail((String) row[5]);
//				user.setPhone((String) row[6]);
//				user.setCenterid((int) row[7]);
//				user.setCentername((String) row[8]);
//				
//			}
//			
//		return user;
//	}
	
	
	//&&&
	
	
	

	
    static StringBuilder dataCache;
    static String [] data;
//    static{
//    
//    dataCache = new StringBuilder();
//    dataCache.append("Aaron Hank,Abagnale Frank,Abbey Edward,Abel Reuben,Abelson Hal,"
//        + "Abourezk James,Abrams Creighton,Ace Jane,Ba Jin,Baba Meher,Baba Tupeni,"
//        + "Babbage Charles,Babbitt Milton,Bacevich Andrew,Bach Richard,Bachelard Gaston,"
//        + "Bachelot Roselyne,Bacon Francis,Baddiel David,Baden-Powell Sir Robert (B-P),"
//        + "Badiou, Alain,Badnarik, Michael,Cabell James Branch,Caesar Irving,Caesar Julius,"
//        + "Cage John,Cain Peter,Callaghan James,Calvin John,Cameron Julia,Cameron Kirk,"
//        + "Java Honk,Java Honk Test,Java Honk Test Successful,Java Honk Spring MVC,"
//        + "Java Honk autocomplete,Java Honk Spring MVC autocomplete List");
//    
//    data =dataCache.toString().split(",");
//    }
    
    public  List<String> getName(String name) {

    List<String> returnMatchName = new ArrayList<String>();
    
//    String[] stockArr = new String[stockList.size()];
//    stockArr = stockList.toArray(stockArr);
    
    String [] data =dataCache.toString().split(",");
    
    
    for (String string : data) {
        if (string.toUpperCase().indexOf(name.toUpperCase())!= -1) {
        returnMatchName.add(string);
        }
    }
    
    return returnMatchName;
    }
    
	public int checkOngoingAssessmentforCustomer(Long userid) {
		Query query = entityManager.createNativeQuery("select * from assessments where userid  = ? and status != 'C' ");
		query.setParameter(1, userid);
		
		return query.getResultList().size();
	}
	
	public void updateLmsidByUserId(Long userid, Long lmsid)
	{
		Query query = entityManager.createNativeQuery("update user set lmsid = ? where userid  = ? ");
		query.setParameter(1, lmsid);
		query.setParameter(2, userid);
		
		query.executeUpdate();
		
	}

}
