package com.skillmatic.service.serviceimpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skillmatic.domain.CourseEnrolment;
import com.skillmatic.jparepository.CourseEnrolmentDao;
import com.skillmatic.service.CourseEnrolmentService;

@Service
@Transactional
public class CourseEnrolementRepositoryImpl implements CourseEnrolmentService {

	private static final Logger LOG = LoggerFactory.getLogger(UserRepositoryImpl.class);
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Autowired
	private CourseEnrolmentDao courseEnrolmentDao;
	

	 public Iterable<CourseEnrolment> findCourseEnrolmentList() {
		 
		return  courseEnrolmentDao.findAll();
		 
	        
	    }
	 public Iterable<CourseEnrolment> findCourseEnrolmentByUserid(Long userId) {
		 
//		return  courseEnrolmentDao.findCourseEnrolmentByCourseid(courseId);
			return  courseEnrolmentDao.findByUserid(userId);
	 
	        
	    }


}


