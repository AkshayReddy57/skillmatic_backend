package com.skillmatic.service.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skillmatic.domain.Institution;
import com.skillmatic.domain.User;
import com.skillmatic.jparepository.InstitutionDao;
import com.skillmatic.jparepository.UserDao;
import com.skillmatic.jparepository.UserDaoCustom;
import com.skillmatic.service.InstitutionService;
import com.skillmatic.service.UserService;

@Service
@Transactional
public class InstitutionRepositoryImpl implements InstitutionService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserRepositoryImpl.class);
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Autowired
	private InstitutionDao institutionDao;
	

	 public Iterable<Institution> findInstitutionsList() {
		 
		return  institutionDao.findAll();
		 
	        
	    }
	
}
