package com.skillmatic.service.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skillmatic.domain.Category;
import com.skillmatic.domain.Course;
import com.skillmatic.jparepository.CategoryDao;
import com.skillmatic.service.CategoryService;


@Service
@Transactional
public class CategoryRepositoryImpl implements CategoryService  {

	private static final Logger LOG = LoggerFactory.getLogger(UserRepositoryImpl.class);
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Autowired
	private CategoryDao categoryDao;
	

	 public Iterable<Category> findCategoryList() {
		 
		return  categoryDao.findAll();
		 
	        
}


	public Iterable<JSONObject> getListofCategoryMenu() {

		// String str = "( 'ID', id,  'LEVEL', lvl,  'CATEGORY', name, 'PARENT', parent )" ;
		
		Query query = entityManager.createNativeQuery("WITH recursive cte(id,  name , parent, Lvl) AS ( " + 
				"    SELECT CT.id, CT.name, (SELECT NAME FROM category cat WHERE cat.ID = CT.parent_category_id ) PARENT, " + 
				"    '1' " + 
				"    FROM category AS CT " + 
				"    WHERE  CT.parent_category_id = ''" + 
				"    UNION ALL " + 
				"    SELECT CT.id, CT.name ," + 
				"    (SELECT NAME FROM category cat WHERE cat.ID = CT.parent_category_id ) PARENT," + 
				"    Lvl + 1 " + 
				"    FROM category as CT " + 
				"    JOIN cte AS cte " + 
				"    ON CT.parent_category_id = cte.Id " + 
				") " + 
				" SELECT json_object( 'ID', id,  'LEVEL', lvl,  'category', name, 'PARENT', parent ) " + 
				" FROM cte ");
		
		// "SELECT json_object( 'ID', id,  'LEVEL', lvl,  'CATEGORY', name, 'PARENT', parent ) " 
		// "SELECT id,  lvl,  name, parent  " 
			System.out.println(" Query" + query.toString() );
			return query.getResultList();

	
	}



}

