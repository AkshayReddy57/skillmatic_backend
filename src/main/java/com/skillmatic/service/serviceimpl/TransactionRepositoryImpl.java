package com.skillmatic.service.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.skillmatic.domain.Transaction;
import com.skillmatic.domain.User;
import com.skillmatic.domain.security.UserRole;
import com.skillmatic.jparepository.TransactionDao;
import com.skillmatic.service.TransactionService;


@Service
@Transactional
public class TransactionRepositoryImpl implements TransactionService  {

	private static final Logger LOG = LoggerFactory.getLogger(TransactionRepositoryImpl.class);
	
	@PersistenceContext
    EntityManager entityManager;
	
	@Autowired
	private TransactionDao transactionDao;

	public void save(Transaction  transaction) {
		transactionDao.save(transaction);
    }
	
	 public Transaction createTransaction(Transaction  transaction) {
		  
	            return transactionDao.save(transaction);
		}

}

