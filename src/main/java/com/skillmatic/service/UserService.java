package com.skillmatic.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.query.Procedure;

import com.skillmatic.domain.User;
import com.skillmatic.domain.security.UserRole;
import com.skillmatic.util.constants.UserStatus;

public interface UserService {
	User findByUsername(String username);

    User findByEmail(String email);
    
    User getUser(String name);
    
    //User getUserforProfile(String emailid, String profiletype);

    boolean checkUserExists(String username, String email);
    boolean checkUserExists(String username);
    
   // boolean checkUserExistsforProfile(String emailid, String profiletype);
    public boolean checkProfileEmailExists(String email);

    boolean checkUsernameExists(String username);

    boolean checkEmailExists(String email);
    
    //boolean checkProfileEmailExists(String email);
    
    void save (User user);
    
    User createUser(User user, Set<UserRole> userRoles);
    
    int updateUser(User user);
    
    User saveUser (User user); 
    
    List<User> findUserList();

    void enableUser (String username);

    void disableUser (String username);
    
    void deleteUserById(Long userId);
	 
   	User findUserById(Long clientId);
   	
   	

   	List<User> findByEnabledUser(UserStatus enable);
   	
   	List<User> findByDisabledUser(UserStatus disable);
   	
   	User findUsersByQuery();
	int updateProfileImgUrl(String profileimgurl, Long userid);
	int updateResumeUrl(String resumeurl, Long userid);
	
	
	int resettemppassword(Long userId, String newpassword) throws Exception;
	int resetpassword(String confirmtokenid, String newpassword) throws Exception;
	
	boolean checkFBSocialIdExists(String socialid);
	boolean checkGPSocialIdExists(String socialid);
	
	User createFBUser(User user,  Set<UserRole> userRoles);
	User createGPUser(User user,  Set<UserRole> userRoles);
	
	User createClientTrainer(User user,  Set<UserRole> userRoles);
	
	User findUserByIdAndCenter(Long userId, String centerId);
	
	public List<User> getListofTrainersByCenter(String centerid);
	
	
	 
	 public String getCentername(String centerid);
	 
	 public boolean checkPhoneforCenterExist(String phone, String centerid);
	 
	 public int checkOngoingAssessmentforCustomer(Long userid);
	 
	 public int activateUser(String confirmtokenid);
	 
	 public int updatePassword(String newpassword, String userid);
	 public int updateConfirmToken(Long userid, String confirmtokenid);
	 
	 public User getUserByEmail(String emailid);
	 
	 public int updateUserDetails(User user);
	 
	 public void updateLmsidByUserId(Long userId, Long lmsid);
	 
}
