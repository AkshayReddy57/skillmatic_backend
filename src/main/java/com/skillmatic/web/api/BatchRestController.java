package com.skillmatic.web.api;

import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import com.skillmatic.domain.Categoryraw;
import com.skillmatic.domain.Courseenrolmentraw;
import com.skillmatic.domain.Courseraw;
import com.skillmatic.domain.Institution;
import com.skillmatic.jparepository.CategoryrawDao;
import com.skillmatic.jparepository.CourseenrolmentrawDao;
import com.skillmatic.jparepository.CourserawDao;
import com.skillmatic.util.Helper;
import  com.skillmatic.service.CategoryService;

@RestController
@RequestMapping("/api")
public class BatchRestController {
	

	@Autowired
	private CourserawDao courserawDao;

	@Autowired
	private CategoryrawDao categoryrawDao;
	
	@Autowired
	private CategoryService categoryService; 
	
	@Autowired
	private CourseenrolmentrawDao courseenrolmentrawDao;
	
	@PersistenceContext
    EntityManager entityManager;
	

@SuppressWarnings("unchecked")

private static final Logger logger = LoggerFactory.getLogger(BatchRestController.class);

			@GetMapping(value="/getcourses",

					produces=MediaType.APPLICATION_JSON_VALUE)

			public ResponseEntity getCourses(){

				RestTemplate restTemplate = new RestTemplate();
				String url = "https://skillmaticindia.talentlms.com/api/v1/courses/";
				HttpHeaders headers = new HttpHeaders();
				headers.set("Accept", "application/json");
				headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
			    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			    ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			    JSONParser parser = new JSONParser();
		        JSONArray jsonArrays = null;
	        	
		        courserawDao.deleteAll();
        		courseenrolmentrawDao.deleteAll();
        		
		        try {
		        	jsonArrays = (JSONArray) parser.parse(result.getBody());
		        	jsonArrays.forEach(item-> {
		        	Courseraw tempcourses = new Courseraw();
		        	tempcourses.setId(Long.valueOf((String)((JSONObject)item).get("id")));
		        	tempcourses.setName((String)((JSONObject)item).get("name"));
		        	tempcourses.setCode((String)((JSONObject)item).get("code"));
		        	tempcourses.setCategory_id((String)((JSONObject)item).get("category_id"));
		        	tempcourses.setDescription((String)((JSONObject)item).get("description"));
		        	tempcourses.setPrice((String)((JSONObject)item).get("price"));
		        	tempcourses.setStatus((String)((JSONObject)item).get("status"));
		        	tempcourses.setCreation_date(Helper.dateConversionStr2DtDDMMYYYYHHMISS((String)((JSONObject)item).get("creation_date")));
		        	tempcourses.setLast_update_on(Helper.dateConversionStr2DtDDMMYYYYHHMISS((String)((JSONObject)item).get("last_update_on")));
		        	tempcourses.setCreator_id(Long.valueOf((String)((JSONObject)item).get("creator_id")));
		        	tempcourses.setHide_from_catalog((String)((JSONObject)item).get("hide_from_catalog"));
		        	tempcourses.setTime_limit((String)((JSONObject)item).get("time_limit"));
		        	tempcourses.setLevel((String)((JSONObject)item).get("level"));
		        	tempcourses.setShared((String)((JSONObject)item).get("shared"));
		        	tempcourses.setShared_url((String)((JSONObject)item).get("shared_url"));
		        	tempcourses.setAvatar((String)((JSONObject)item).get("avatar"));
		        	tempcourses.setBig_avatar((String)((JSONObject)item).get("big_avatar"));
		        	tempcourses.setCertification((String)((JSONObject)item).get("certification"));
		        	tempcourses.setCertification_duration((String)((JSONObject)item).get("certification_duration"));
		        	
		        	courserawDao.save(tempcourses);

		        	
		        	{
		        	

		    			RestTemplate restTemplateen = new RestTemplate();
						String urlen = "https://skillmaticindia.talentlms.com/api/v1/courses/id:"+Long.valueOf((String)((JSONObject)item).get("id"));
						System.out.println(" ** ENRL ** " + urlen);
						HttpHeaders headersen = new HttpHeaders();
						headersen.set("Accept", "application/json");
						headersen.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
					    HttpEntity<String> entityen = new HttpEntity<String>("parameters", headersen);
					    ResponseEntity<String> resulten = restTemplateen.exchange(urlen, HttpMethod.GET, entityen, String.class);
					    JSONParser parseren = new JSONParser();
				    	try {
				    		
				    		JSONObject item1 = (JSONObject) parseren.parse(resulten.getBody());
				    		

				    			JSONArray usersArray = ((JSONArray)((JSONObject)item1).get("users"));
				    		
				    			usersArray.forEach(user-> System.out.println("AAA"+ user));
				    			usersArray.forEach(user-> {
				    				Courseenrolmentraw tempcoursesenrl = new Courseenrolmentraw();
				    							tempcoursesenrl.setCourseid(Long.valueOf((String)((JSONObject)item).get("id")));
				    							tempcoursesenrl.setUserid(Long.valueOf((String)((JSONObject)user).get("id"))); 
				    							tempcoursesenrl.setRole((String)((JSONObject)user).get("role"));
				    							tempcoursesenrl.setTotal_time((String)((JSONObject)user).get("total_time"));
				    							tempcoursesenrl.setCompletion_percentage(Long.valueOf((String)((JSONObject)user).get("completion_percentage"))); 
				    							tempcoursesenrl.setName((String)((JSONObject)user).get("name"));
				    							tempcoursesenrl.setEnrolled_on(Helper.dateConversionStr2DtDDMMYYYYHHMISS((String)((JSONObject)user).get("enrolled_on")));
				    							courseenrolmentrawDao.save(tempcoursesenrl);

				    			});
				    			
				    		
				    		
				    		
				    		} catch (Exception e) {
				    		
//				    		e.printStackTrace();
				    			logger.debug("Error in courses batch >> " + e.getMessage());
				    		
				    		}
				    			
				        
		        	}
		        	
		        	System.out.println("UUUU>> " + tempcourses.toString());

		        	});
		           

		        } catch (Exception e) {

		        	logger.debug("Error in courses batch setp 2 >> " + e.getMessage());
		        }
		        
		        {
		        	RestTemplate restTemplatect = new RestTemplate();
					String urlct = "https://skillmaticindia.talentlms.com/api/v1/categories/";
					HttpHeaders headersct = new HttpHeaders();
					headersct.set("Accept", "application/json");
					headersct.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
				    HttpEntity<String> entityct = new HttpEntity<String>("parameters", headers);
				    ResponseEntity<String> resultct = restTemplatect.exchange(urlct, HttpMethod.GET, entityct, String.class);
				    JSONParser parserct = new JSONParser();
			        JSONArray jsonArraysct = null;
		        	
			        categoryrawDao.deleteAll();
	        		
			        try {
			         	jsonArraysct = (JSONArray) parser.parse(resultct.getBody());
			        
			        	jsonArraysct.forEach(itemct-> {
			        	Categoryraw tempcategory = new Categoryraw();
			        	tempcategory.setId(Long.valueOf((String)((JSONObject)itemct).get("id")));
			        	System.out.println("after ID");
			        	tempcategory.setName((String)((JSONObject)itemct).get("name"));
			        	System.out.println("after Name");
			        	tempcategory.setPrice((String)((JSONObject)itemct).get("price"));
			        	tempcategory.setParent_category_id(Optional.ofNullable((String)((JSONObject)itemct).get("parent_category_id")).orElse(""));
			        	
			        //	System.out.println("Parent ID" + (String)((JSONObject)itemct).get("parent_category_id"));
			        	
			        	String parentID =  Optional.ofNullable((String)((JSONObject)itemct).get("parent_category_id")).orElse("");
			         	System.out.println(" Assigned " + parentID + " --- CHECK --- ") ;
			         	
			        	categoryrawDao.save(tempcategory);

			        	});
		    			
			    		
			    		
			    		
		    		} catch (ParseException e) {
		    		
		    			logger.debug("Error in get categories batch >> " + e.getMessage());
		    		
		    		}
		        }
				
		    	//  @Query(nativeQuery = true,value = "call batchdatamove");
//		    		@Data
//		    		@NamedStoredProcedureQueries({
//		    		        @NamedStoredProcedureQuery(name = "batch_datamove",
//		    		                                    procedureName = "batchdatamove")});
//		    		
//		    		@Procedure(name = "batchdatamove")
//		    		batchdatamove();
		    //	
		    		
		        	courserawDao.batchdatamove();
		        
			    logger.debug("JSON VALUE >>  " + result);
			    logger.debug("get courses from LMS >> " + result );
			    	
				return new ResponseEntity("",HttpStatus.OK);

			}   


@SuppressWarnings("unchecked")

// private static final Logger logger = LoggerFactory.getLogger(UserRestController.class);

			@GetMapping(value="/callbatch",

					produces=MediaType.APPLICATION_JSON_VALUE)

			public ResponseEntity callBatch(){

			/*	RestTemplate restTemplate = new RestTemplate();
				String url = "https://caerus.talentlms.com/api/v1/courses/";
				HttpHeaders headers = new HttpHeaders();
				headers.set("Accept", "application/json");
				headers.add("Authorization", "Basic NTFOWlJhS2JCeXdyZDlSNHNlNVZpeTVmYzVTaUtyOg==");
			    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			    ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			    JSONParser parser = new JSONParser();
		        JSONArray jsonArrays = null;
	        	*/
	
		    	//  @Query(nativeQuery = true,value = "call batchdatamove");
//		    		@Data
//		    		@NamedStoredProcedureQueries({
//		    		        @NamedStoredProcedureQuery(name = "batch_datamove",
//		    		                                    procedureName = "batchdatamove")});
//		    		
//		    		@Procedure(name = "batchdatamove")
//		    		batchdatamove();
//		    //	
//		    		System.out.println("before Proce call ");
//		    		
//		    			Query query = entityManager.createNativeQuery("call batchdatamove()");
//		    		query.executeUpdate();
//		    		System.out.println("after Proce call ");
//		    		

			   // logger.debug("JSON VALUE >>  " + result);
			    //logger.debug("get courses from LMS >> " + result );

	

			courserawDao.batchdatamove();

			return new ResponseEntity("",HttpStatus.OK);

			}   

			
		@GetMapping(value="/getcategory",
				produces=MediaType.APPLICATION_JSON_VALUE)

		public ResponseEntity<Iterable<JSONObject>> getListofCategoryMenu(){
				Iterable<JSONObject> category = categoryService.getListofCategoryMenu();
				return new ResponseEntity<Iterable<JSONObject>>(category,HttpStatus.OK);
			}

			
			
	@GetMapping(value="/getcoursesbyid/{id}",

		produces=MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity getCoursesById(@PathVariable("id") String id){

		RestTemplate restTemplate = new RestTemplate();
		System.out.println("ABCD>>  " + id);
		String url = "https://skillmaticindia.talentlms.com/api/v1/courses/id:"+id;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
		JSONParser parser = new JSONParser();

		JSONArray jsonArrays = null;
		try {
		
		JSONObject item = (JSONObject) parser.parse(result.getBody());
		
			System.out.println(((JSONObject)item).get("id"));
		
			System.out.println(((JSONObject)item).get("price"));
		
			JSONArray usersArray = ((JSONArray)((JSONObject)item).get("users"));
		
			usersArray.forEach(user-> System.out.println("AAA"+ user));
			usersArray.forEach(user-> {
							System.out.println("AAA 00 "+((JSONObject)user).get("id"));
							System.out.println("AAA 00 "+((JSONObject)user).get("role"));
							System.out.println("AAA 00 "+((JSONObject)user).get("enrolled_on"));
							System.out.println("AAA 00 "+((JSONObject)user).get("name"));
			});
			
		
		
		
		} catch (ParseException e) {
		
			logger.debug("Error in get course by id batch >> " + e.getMessage());
		
		}

		
		return new ResponseEntity("",HttpStatus.OK);
		
		
		
	}					
		

}