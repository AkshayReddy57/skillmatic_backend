package com.skillmatic.web.api;

import java.security.Principal;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.skillmatic.domain.User;
import com.skillmatic.service.UserService;
import com.skillmatic.util.Helper;
import com.skillmatic.util.constants.UserStatus;

import com.ccavenue.security.*;

@Controller
//@RequestMapping("/api")
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    
	@Autowired
	private Helper helper;
	
	@RequestMapping("/api/ccavenue")
    public String ccavenue() {
        return "dataFrom";
    }

	
	@PostMapping("/api/ccavenuerequest")
    public String ccavenuerequest(Model model, HttpServletRequest request, 
            HttpServletResponse response) {
		
		
		 String merchantId = "162062";   
		 String accessCode = "AVPS78FE15BT23SPTB";	// Put in the Access Code provided by CCAVENUES
		 String workingKey = "50601535C0C9EF35F48F89098012697C";    // Put in the Working Key provided by CCAVENUES								 
		                                                            
		 Enumeration enumeration=request.getParameterNames();
		 String ccaRequest="", pname="", pvalue="";
		 while(enumeration.hasMoreElements()) {
		      pname = ""+enumeration.nextElement();
		      pvalue = request.getParameter(pname);
		      ccaRequest = ccaRequest + pname + "=" + pvalue + "&";
		      System.out.println("pre testing " + ccaRequest);
		 }
		 AesCryptUtil aesUtil=new AesCryptUtil(workingKey);
		 String encRequest = aesUtil.encrypt(ccaRequest);
		 
		 
	//	 String src = "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction" + "&merchant_id=" +
	//			 merchantId + "&encRequest="+ encRequest + "&access_code=" + accessCode;
		 
		model.addAttribute("merchantId", merchantId );
		model.addAttribute("accessCode", accessCode);
		model.addAttribute("encRequest", encRequest );
		
	//	model.addAttribute("key", src);
		
		System.out.println("TESTS>>>" + encRequest);
        return "ccavRequestHandler";
    }
	

	
	
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(Principal principal, Model model) {
        User user = userService.findByUsername(principal.getName());
     
     
        
        model.addAttribute("user", user);

        return "profile";
    }

 
    
    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String profilePost(@Valid @ModelAttribute("user") User newUser, BindingResult bindingResult, 
    		Model model, Principal principal) {

    	if (bindingResult.hasErrors()) {
			 return "profile";
	        }
    	
        User user = userService.findByUsername(newUser.getUsername());
    //    user.setUsername(newUser.getUsername());
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());
        user.setEmail(newUser.getEmail());
        user.setPhone(newUser.getPhone());
        
        
        user.setProfileimgurl("a");
       // user.setPrimarySkill(newUser.getPrimarySkill());
        
     //   user.setSocialid("a");
        user.setSource("a");
     //   user.setAdditionalSkill1(newUser.getAdditionalSkill1());
//        user.setAdditionalSkill2(newUser.getAdditionalSkill2());
//        user.setAboutme("a");
//        user.setMembersince(new Date());
        
        
        
        
        
        
        //model.addAttribute("success", "true");
//        model.addAttribute("user", user);

        userService.saveUser(user);
//        List<Skills> Skillslist = Arrays.asList(Skills.values());
//        model.addAttribute("Skillslist", Skillslist);
//        model.addAttribute("selectedprimaryskill", newUser.getPrimarySkill());
//        model.addAttribute("selectedadditionalSkill1", newUser.getAdditionalSkill1());
//        model.addAttribute("selectedadditionalSkill2", newUser.getAdditionalSkill2());		
	     
   
		 return "profile";
    }
    
    @RequestMapping(value = "/profile/list", method = RequestMethod.GET)
    public String userList(Model model, Principal principal) {
	

		List<User> userlist = userService.findUserList();
	
        model.addAttribute("userlist", userlist);

        return "profileuserlist";
    }
    
	@RequestMapping(value = "/profile/edit", method = RequestMethod.GET)
	public String userEdit(@RequestParam(value = "id") String userId, Model model, Principal principal,HttpServletRequest request){

		User user= userService.findUserById(Long.parseLong(userId));

		model.addAttribute("user", user);

		return "profile";
	}
    
    @RequestMapping(value = "/profile/deactivate", method = RequestMethod.GET)
    public String profiledeactivate(Principal principal, Model model,@ModelAttribute("user") User newUser,@RequestParam(value = "id") String userId) {
    
    	User user= userService.findUserById(Long.parseLong(userId));
  
    	user.setEnabled(false);
    	logger.debug("deactivated" +user);
    	userService.saveUser(user);
        model.addAttribute("deactivate", true);
        
        model.addAttribute("user", user);
             
        return "profile";
        
    }
    @RequestMapping(value = "/profile/active", method = RequestMethod.GET) 
    public String profileActiveUsers(Principal principal, Model model,@ModelAttribute("user") User newUser){
    	
    	List<User> userEnable=userService.findByEnabledUser(UserStatus.Enable);
    	
    	List<User> userDisable =userService.findByDisabledUser(UserStatus.Disable);
    	model.addAttribute("userEnable", userEnable);
    	model.addAttribute("userDisable", userDisable);
    	return "listuser";
    	
   	
    	
    }
   
}