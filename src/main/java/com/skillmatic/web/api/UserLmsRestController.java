package com.skillmatic.web.api;

import java.security.Principal;
import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.skillmatic.util.APIResponse;
import com.skillmatic.util.Helper;

import com.skillmatic.domain.User;
import com.skillmatic.service.UserService;
import com.skillmatic.domain.Course;

@RestController
@RequestMapping("/api")
public class UserLmsRestController {

	private static final Logger logger = LoggerFactory.getLogger(UserLmsRestController.class);

	@Autowired
	 private Helper helper;

	@Autowired
	 private UserService userService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST, value = "/enrolcourse", consumes = "application/json")
	public void enrolCourse(@RequestBody String courseenrl, BindingResult bindingResult, Model model,
			Principal principal) {

		try {
			JSONObject result = helper.getGeneric(courseenrl);
			// System.out.println("-- - " + result.toJSONString());
			JSONArray coursesArray = ((JSONArray) ((JSONObject) result).get("courses"));
			JSONObject userobj = new JSONObject();
			Long lmsid;
			Long tblLmsid;

			// Create User in LMS
			if (coursesArray.isEmpty() == false) {

				Long userid = Long.valueOf((String) ((JSONObject) result).get("userid"));
				// Create User in LMS
				User userdtl = new User();
				userdtl = userService.findUserById(userid);
				System.out.println(" Before");

				tblLmsid = userdtl.getLmsId();
				String lmsidstr = tblLmsid + "";

				userobj.put("first_name", userdtl.getFirstName());
				userobj.put("last_name", userdtl.getLastName());
				userobj.put("email", userdtl.getEmail());
				userobj.put("login", userdtl.getUsername());
				userobj.put("password", userdtl.getUserId() + "mypass");

				if (lmsidstr.equals("null")) {

					RestTemplate restTemplate = new RestTemplate();
					String url = "https://skillmaticindia.talentlms.com/api/v1/usersignup/";
					HttpHeaders headers = new HttpHeaders();
					headers.set("Accept", "application/json");
					headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
					HttpEntity<String> entity = new HttpEntity<String>(userobj.toJSONString(), headers);
					System.out.println(" entity " + entity.toString());
					// ResponseEntity<String> resultfromLMS = restTemplate.exchange(url,
					// HttpMethod.POST, entity, String.class);

					ResponseEntity<JSONObject> resultfromLMSJ = restTemplate.exchange(url, HttpMethod.POST, entity,
							JSONObject.class);

					JSONObject lmsresponse = resultfromLMSJ.getBody();

					lmsid = Long.valueOf((String) (lmsresponse.get("id").toString()));
					userService.updateLmsidByUserId(userid, lmsid);
					tblLmsid = lmsid;
				} else {
					lmsid = tblLmsid;

					System.out.println(" inside else ");
				}

				// Courses for Enrolment
				coursesArray.forEach(courses -> {

					System.out.println("Course ID : - Enrol " + courses.toString());

					Long courseid = Long.valueOf((String) ((JSONObject) courses).get("courseid"));
					System.out.println("Course ID : - Enrol " + courseid);
					String userrole = null;
					System.out.println("Role " + result.get("role") + result.get("role") == "ROLE_SEEKER");
					// if (result.get("role")== "ROLE_SEEKER")
					{
						userrole = "learner";
					}
					JSONObject courseobj = new JSONObject();

					courseobj.put("user_id", lmsid);
					courseobj.put("course_id", courseid);
					courseobj.put("role", userrole);

					RestTemplate restTemplatecr = new RestTemplate();
					String urlcr = "https://skillmaticindia.talentlms.com/api/v1/addusertocourse/";
					HttpHeaders headerscr = new HttpHeaders();
					headerscr.set("Accept", "application/json");
					headerscr.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
					HttpEntity<String> entitycr = new HttpEntity<String>(courseobj.toJSONString(), headerscr);
					System.out.println(" entitycr " + entitycr.toString());
					ResponseEntity<String> resultfromLMS = restTemplatecr.exchange(urlcr, HttpMethod.POST, entitycr,
							String.class);

					System.out.println(" AFTER Course enrol -- " + resultfromLMS.toString());

				});

			}

		} catch (Exception e) {
			logger.debug("Error in register >> " + e.getMessage());
		}

	}
	
	
	
	//
	
	
	@PostMapping(value="/createdummyuser/{userid}",
			produces=MediaType.APPLICATION_JSON_VALUE)
						public void createDummyUser(@PathVariable("userid") String userid){
				System.out.println("in the call 1");

				User user = userService.findUserById(Long.valueOf(userid));
				System.out.println (" Before");
				
				//String lmsid = String.valueOf(user.getLmsId());
				
				JSONObject userobj = new JSONObject();
				
				userobj.put("first_name", user.getFirstName());
				userobj.put("last_name", user.getLastName());
				userobj.put("email", user.getEmail());
				userobj.put("login", user.getUsername());
				userobj.put("password", user.getUserId() + "mypass");

				if (user.getLmsId() == null) { 

					RestTemplate restTemplate = new RestTemplate();
			    	String url = "https://skillmaticindia.talentlms.com/api/v1/usersignup/";
					HttpHeaders headers = new HttpHeaders();
					headers.set("Accept", "application/json");
					headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
				    HttpEntity<String> entity = new HttpEntity<String>(userobj.toJSONString(), headers);
				  
				    ResponseEntity<JSONObject> resultfromLMSJ = restTemplate.exchange(url, HttpMethod.POST, entity, JSONObject.class);

				    JSONObject lmsresponse = 	resultfromLMSJ.getBody();
					   
				   
				   	userService.updateLmsidByUserId(Long.valueOf(userid), Long.valueOf((String)(lmsresponse.get("id").toString())));
				    	
				
			}
				
	}
	
	
	
	//
	
	
}