package com.skillmatic.web.api;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.skillmatic.domain.Institution;
import com.skillmatic.domain.User;
import com.skillmatic.domain.security.UserRole;
import com.skillmatic.jparepository.RoleDao;
import com.skillmatic.service.UserService;
import com.skillmatic.service.InstitutionService;
import com.skillmatic.util.APIResponse;
import com.skillmatic.util.APIResponseObj;
import com.skillmatic.util.Helper;
import com.skillmatic.util.MailClient;


	@RestController
	@RequestMapping("/api")
	public class CommonsRestController {
		private static final Logger logger = LoggerFactory.getLogger(UserRestController.class);
		
		private static final String SALT = "salt"; // Salt should be protected carefully
		
		@Autowired
		UserService userService;
		
		@Autowired
		InstitutionService institutionService;
		
		
		@Autowired
		 private Helper helper;
		
		@Autowired
	    private RoleDao roleDao;
		
		@Autowired
		private MailClient mailclient;
		
		
		@GetMapping(value="/getallinstitutions",
				produces=MediaType.APPLICATION_JSON_VALUE)

		public ResponseEntity<Iterable<Institution>> findInstitutionsList(){
			Iterable<Institution> institutions = institutionService.findInstitutionsList();
			return new ResponseEntity<Iterable<Institution>>(institutions,HttpStatus.OK);
		}
	
		

	
		
		@RequestMapping(
	            value = "/sendotp",
	            method = RequestMethod.POST,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity sendotp( @RequestBody String phonenumber, BindingResult bindingResult ) {
			
			
			logger.debug("sign in json:::::::;  " + phonenumber);
			String resultBody = "";
			ResponseEntity<String> result = null;
			OTP otp = new OTP();
			
		     try {    
		    	 
		    	 RestTemplate restTemplate = new RestTemplate();
					
					String url = "https://2factor.in/API/V1/5ad8fefc-43b0-11e8-a895-0200cd936042/SMS/" + phonenumber + "/AUTOGEN";
					HttpHeaders headers = new HttpHeaders();

				    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
				    result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
				   
				    System.out.println("AA 1 >> " + result.getBody());
				    
				    // when
				    JSONObject object = (JSONObject) JSONValue.parse(result.getBody());
				    otp.setStatus((String)object.get("Status"));
				    otp.setDetails((String)object.get("Details"));
				    
		     } catch (Exception e) {
		    	 	logger.debug("Error while processing recover password "+ e.getMessage());
		    		return new ResponseEntity<>(new APIResponse("ERROR", HttpStatus.OK, ""), HttpStatus.OK);
		     }
			
		     return new ResponseEntity<>(new APIResponseObj("SUCCESS", HttpStatus.OK, "OTP Sent", otp), HttpStatus.OK);
						
		}
		
		
		
		
		@RequestMapping(
	            value = "/verifyotp/{otpsessionid}/{enteredotp}",
	            method = RequestMethod.POST,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity verifyotp( @PathVariable("otpsessionid") String otpsessionid, 
				@PathVariable("enteredotp") String enteredotp ) {
			
			logger.debug("sign in sessiondetails:::::::;  " + otpsessionid + "entered otp " + enteredotp);
			String resultBody = "";
			ResponseEntity<String> result = null;
			OTP otp = new OTP();
			
		     try {    
		    	 
		    	 RestTemplate restTemplate = new RestTemplate();
		    	 //https://2factor.in/API/V1/{api_key}/SMS/VERIFY/{session_id}/{otp_input}
		    	 
		    	String url = "https://2factor.in/API/V1/5ad8fefc-43b0-11e8-a895-0200cd936042/SMS/VERIFY/" + otpsessionid + "/" + enteredotp;
	    	 	HttpHeaders headers = new HttpHeaders();

			    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			    result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			   
			    System.out.println("AA 1 >> " + result.getBody());
			    
			    // when
			    JSONObject object = (JSONObject) JSONValue.parse(result.getBody());
			    otp.setStatus((String)object.get("Status"));
			    otp.setDetails((String)object.get("Details"));
				    
		     } catch (Exception e) {
		    	 	logger.debug("Error while processing recover password "+ e.getMessage());
		    		return new ResponseEntity<>(new APIResponse("ERROR", HttpStatus.OK, ""), HttpStatus.OK);
		     }
			
		     return new ResponseEntity<>(new APIResponseObj("SUCCESS", HttpStatus.OK, "OTP Verified", otp), HttpStatus.OK);
						
		}
		


//		@RequestMapping(
//	            value = "/sendotp",
//	            method = RequestMethod.POST,
//	            produces = MediaType.APPLICATION_JSON_VALUE)
//		
//			public ResponseEntity sendotp( @RequestBody String stringToParse, BindingResult bindingResult ) {
//			
//			logger.debug("kkkkkkkkkkkkkkkkkkkk:::::::;  " + stringToParse);
//		        
//				return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, ""), HttpStatus.OK);	 
//			
//		}
		
//	
//			
//			@GetMapping(value="/getcourses",
//					produces=MediaType.APPLICATION_JSON_VALUE)
//			//@CrossOrigin(origins= "http://localhost:4200")
//			public ResponseEntity getCourses(){
//				RestTemplate restTemplate = new RestTemplate();
//				
//				String url = "https://caerus.talentlms.com/api/v1/courses/";
//				HttpHeaders headers = new HttpHeaders();
//				headers.set("Accept", "application/json");
//				headers.add("Authorization", "Basic NTFOWlJhS2JCeXdyZDlSNHNlNVZpeTVmYzVTaUtyOg==");
//			    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
//			    ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//			    
//			    logger.debug("JSON VALUE >>  " + result);
//				
//				return new ResponseEntity("",HttpStatus.OK);
//				
//			}
			
		   public JSONObject getGeneric( String stringToParse){
		        JSONParser parser = new JSONParser();
		        JSONObject result = null;
		        JSONArray jsonArray = null;
		        String email = null;
		        
		        try {
		        	jsonArray = (JSONArray) parser.parse(stringToParse);
		            
		            result = (JSONObject)jsonArray.get(0);
		            
		        } catch (ParseException e) {
		            e.printStackTrace();
		        }
		        return result;
		    } 	

			
	}
	
	
class OTP {

	private String status;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	private String details;
	
	
}
	