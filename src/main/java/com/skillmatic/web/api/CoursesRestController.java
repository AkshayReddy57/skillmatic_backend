package com.skillmatic.web.api;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.skillmatic.domain.Course;
import com.skillmatic.domain.CourseEnrolment;
import com.skillmatic.domain.User;
import com.skillmatic.service.CourseService;
import com.skillmatic.util.APIResponse;
import com.skillmatic.service.CategoryService; 
import com.skillmatic.service.CourseEnrolmentService;
import com.skillmatic.service.CourseService;

@RestController
@RequestMapping("/api")
public class CoursesRestController {


	private static final Logger logger = LoggerFactory.getLogger(UserRestController.class);
	
	
	@Autowired
	public CourseService courseService;
	
	@Autowired
	public CategoryService categoryService;
	
	@Autowired
	public CourseEnrolmentService courseEnrolmentService;
	
	@Autowired
	CourseService coursesService;
	

	@SuppressWarnings("unchecked")
	@GetMapping(value="/searchcourses",
	produces=MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Iterable<Course>> findCourseList(){
		Iterable<Course> course = courseService.findCourseList();
		return new ResponseEntity<Iterable<Course>>(course,HttpStatus.OK);
	}

	@GetMapping(value="/searchenrolment",
			produces=MediaType.APPLICATION_JSON_VALUE)

			public ResponseEntity<Iterable<CourseEnrolment>> findCourseEnrolmentList(){
				Iterable<CourseEnrolment> courseEnrolment = courseEnrolmentService.findCourseEnrolmentList();
				return new ResponseEntity<Iterable<CourseEnrolment>>(courseEnrolment,HttpStatus.OK);
			}

	@GetMapping(value="/searchenrolmentbyuser/{userId}",
			produces=MediaType.APPLICATION_JSON_VALUE)

			public ResponseEntity<Iterable<CourseEnrolment>> findCourseEnrolmentByUserid(@PathVariable("userId") Long userId){
				Iterable<CourseEnrolment> courseEnrolment = courseEnrolmentService.findCourseEnrolmentByUserid(userId);
				return new ResponseEntity<Iterable<CourseEnrolment>>(courseEnrolment,HttpStatus.OK);
			}

	@GetMapping(value="/searchcoursebykey/{keyWord}",
			produces=MediaType.APPLICATION_JSON_VALUE)
						public ResponseEntity<Iterable<Course>> getListofCoursesByKeyword(@PathVariable("keyWord") String keyWord){
				System.out.println("in the call");
				Iterable<Course> course = courseService.getListofCoursesByCategory(keyWord);
				return new ResponseEntity<Iterable<Course>>(course,HttpStatus.OK);
			}
	
	@GetMapping(value="/searchcoursebycategoryid/{id}",
			produces=MediaType.APPLICATION_JSON_VALUE)
						public ResponseEntity<Iterable<Course>> getListofCoursesByCategoryID(@PathVariable("id") Long id){
				System.out.println("in the call");
				Iterable<Course> course = courseService.getListofCoursesByCategoryID(id);
				return new ResponseEntity<Iterable<Course>>(course,HttpStatus.OK);
			}
	

	@GetMapping(value="/searchcoursebycategoryname/{category}",
			produces=MediaType.APPLICATION_JSON_VALUE)
						public ResponseEntity<Iterable<Course>> getListofCoursesByCategory(@PathVariable("category") String category){
				System.out.println("in the call");
				Iterable<Course> course = courseService.getListofCoursesByCategory(category);
				return new ResponseEntity<Iterable<Course>>(course,HttpStatus.OK);
			}
	


	@GetMapping(value="/searchcoursebyid/{courseId}",
			produces=MediaType.APPLICATION_JSON_VALUE)

			public ResponseEntity<Iterable<Course>> findCourseByCourseid(@PathVariable("courseId") Long courseId){

				Iterable<Course> course = courseService.getCourseByCourseid(courseId);
				return new ResponseEntity<Iterable<Course>>(course,HttpStatus.OK);
			}

	/*
	@GetMapping(value="/usercourses/{username}",
			produces=MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity usercourses(@PathVariable("username") String username){
		
		RestTemplate restTemplate = new RestTemplate();
    	String url = "https://skillmaticindia.talentlms.com/api/v1/users/";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
		JSONObject jobj = null;
		
		try {
	    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
	       map.add("username", username);
	       
		
	    HttpEntity<MultiValueMap<String, String>> entity = new  HttpEntity<MultiValueMap<String, String>>(map, headers);
	    ResponseEntity<String> resultfromLMS = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
	    
	    System.out.println("Assert that > " + resultfromLMS.getStatusCode());
	    
	    
	    logger.debug("get test from LMS >> " + resultfromLMS );
	    
	    
	    
	    
	    	JSONParser parser = new JSONParser();
		    
		    jobj= (JSONObject)parser.parse(resultfromLMS.getBody());
		    logger.debug ("Usercourses > " + jobj);
		    
		    
		    
	    } catch (Exception e) {
	    	System.out.println("Exception ?>> " + e.getMessage());
	    	 return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, jobj.toJSONString()), HttpStatus.OK);	
	    }
	    
		
	    return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, jobj.toJSONString()), HttpStatus.OK);	
	}
	*/
	

	
	

	@GetMapping(value="/usercourses/{username}",
			produces=MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity usercourses(@PathVariable("username") String username){
		
		RestTemplate restTemplate = new RestTemplate();
    	String url = "https://skillmaticindia.talentlms.com/api/v1/users/";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
		JSONObject jobj = null;
		JSONArray arraay;
		ArrayList<String> arr = new ArrayList<String>();
		ArrayList<JSONObject> finalArr = new ArrayList<JSONObject>();
		
		try {
	    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
	       map.add("username", username);
	       
		
	    HttpEntity<MultiValueMap<String, String>> entity = new  HttpEntity<MultiValueMap<String, String>>(map, headers);
	    ResponseEntity<String> resultfromLMS = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
	    
	    System.out.println("Assert that > " + resultfromLMS.getStatusCode());
	    
	    
	    logger.debug("get test from LMS >> " + resultfromLMS );
	    
	    
	    
	    
	    	JSONParser parser = new JSONParser();
		    
		    jobj= (JSONObject)parser.parse(resultfromLMS.getBody());
		    logger.debug ("JONJ > " + jobj);
		    
		    arraay = (JSONArray)jobj.get("courses");
		   // JSONObject abc = (JSONObject)arraay.get(1);
		    
		    for (int i=0; i<arraay.size() ; i++) 
		    { 
		    	JSONObject abc = (JSONObject)arraay.get(i);
		    	arr.add((String) abc.get("id"));
		    }
	
		    
		    logger.debug ("JSON arrayy LIST > " + arr);
		    
		    
		    for (int i=0; i<arr.size() ; i++) 
		    { 

		    	
		    	RestTemplate restTemplate1 = new RestTemplate();
		    	String url1 = "https://skillmaticindia.talentlms.com/api/v1//courses/";
				HttpHeaders headers1 = new HttpHeaders();
				headers1.set("Accept", "application/json");
				headers1.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
				
				
				try {

				    MultiValueMap<String, String> map1 = new LinkedMultiValueMap<String, String>();
				       map1.add("id", arr.get(i));
			       

			HttpEntity<MultiValueMap<String, String>> entity1 = new  HttpEntity<MultiValueMap<String, String>>(map1, headers1);
			   ResponseEntity<String> resultfromLMS1 = restTemplate1.exchange(url1, HttpMethod.POST, entity1, String.class);
				    
				    System.out.println("Assert that > " + resultfromLMS1.getStatusCode());
				    

			    	JSONParser parser1 = new JSONParser();
				    
				    JSONObject jobj1= (JSONObject)parser1.parse(resultfromLMS1.getBody());
				   

				    finalArr.add(jobj1);
					
				
				    logger.debug ("ARRR obj > " + finalArr);
				
				} catch (Exception e) {
			    	System.out.println("Exception ?>> " + e.getMessage());
			    }
		    	
		    	
		    }
		    
		    
		    
	    } catch (Exception e) {
	    	System.out.println("Exception ?>> " + e.getMessage());
	    	// return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, nn), HttpStatus.OK);	
	    }

		
	    return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, finalArr.toString()), HttpStatus.OK);	
	}
	
	
	@GetMapping(value="/userenrolled/{username}/{courseid}",
			produces=MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity userenrolled(@PathVariable("username") String username, @PathVariable("courseid") String courseid){
		
		RestTemplate restTemplate = new RestTemplate();
    	String url = "https://skillmaticindia.talentlms.com/api/v1/users/";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
		JSONObject jobj = null;
		JSONArray arraay;
		ArrayList<String> arr = new ArrayList<String>();
		String enrolled = "";
		String usrId = "";
		String authurl = "";
		
		try {
	    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
	       map.add("username", username);
	       
		
	    HttpEntity<MultiValueMap<String, String>> entity = new  HttpEntity<MultiValueMap<String, String>>(map, headers);
	    ResponseEntity<String> resultfromLMS = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
	    
	    System.out.println("Assert that > " + resultfromLMS.getStatusCode());
	    
	    
	    logger.debug("get test from LMS >> " + resultfromLMS );
	    
	    
	    
	    
	    	JSONParser parser = new JSONParser();
		    
		    jobj= (JSONObject)parser.parse(resultfromLMS.getBody());
		    logger.debug ("JONJ > " + jobj);
		    
		    arraay = (JSONArray)jobj.get("courses");
		    usrId = (String)jobj.get("id");
		    
		    logger.debug(usrId + '/' + courseid);
		   // JSONObject abc = (JSONObject)arraay.get(1);
		    
		    for (int i=0; i<arraay.size() ; i++) 
		    { 
		    	JSONObject abc = (JSONObject)arraay.get(i);
		    	arr.add((String) abc.get("id"));
		    }
	
		    
		    logger.debug ("JSON arrayy LIST > " + arr);
		    logger.debug ("JSON arrayy LIST > " + arr.get(0).equals(courseid));
		    
		    
		    for (int i=0; i<arr.size() ; i++) 
		    { 
		    	if(arr.get(i).equals(courseid)) {
		    		enrolled = "true";
		    		


		    		RestTemplate restTemplate1 = new RestTemplate();
		        	String url1 = "https://skillmaticindia.talentlms.com/api/v1/gotocourse/";
		    		HttpHeaders headers1 = new HttpHeaders();
		    		headers1.set("Accept", "application/json");
		    		headers1.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
		    		
		    		
		    		try {
		    			
		    		    MultiValueMap<String, String> map1 = new LinkedMultiValueMap<String, String>();
		    		       map1.add("user_id", usrId );
		    		       map1.add("course_id", courseid );
		    		       
		    			
		    		    HttpEntity<MultiValueMap<String, String>> entity1 = new  HttpEntity<MultiValueMap<String, String>>(map1, headers1);

		    		    System.out.println("Assert that id > " + entity1);
		    		    
		    		    ResponseEntity<String> resultfromLMS1 = restTemplate.exchange(url1, HttpMethod.POST, entity1, String.class);
		    		    
		    		    System.out.println("Assert that > " + resultfromLMS1.getStatusCode());
		    		    
		    		    
		    		    logger.debug("get test from LMS >> " + resultfromLMS1 );
		    		    
		    		    	JSONParser parser1 = new JSONParser();
		    			    
		    			    JSONObject jobj1= (JSONObject)parser.parse(resultfromLMS1.getBody());
		    			    
		    			    authurl = (String)jobj1.get("goto_url");
		    			    
		    			    System.out.println("URL ?>> " + authurl);
		    			    
		    		    } catch (Exception e) {
		    		    	System.out.println("Exception ?>> " + e.getMessage());
		    		    	 //return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, authurl), HttpStatus.OK);	
		    		    }

		    		}else {
		    		enrolled = "false";
		    	}
		    }
		    
		    
		    
		    logger.debug("enrolled ?>> " + enrolled);
		    
	    } catch (Exception e) {
	    	System.out.println("Exception ?>> " + e.getMessage());
	    	// return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, nn), HttpStatus.OK);	
	    }

		
	    return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, authurl), HttpStatus.OK);	
	}
	
	
	
	
}