package com.skillmatic.web.api;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.ccavenue.security.AesCryptUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skillmatic.domain.Category;
import com.skillmatic.domain.Courseenrolmentraw;
import com.skillmatic.domain.Courseraw;
import com.skillmatic.domain.User;
import com.skillmatic.domain.security.UserRole;
import com.skillmatic.jparepository.CategoryDao;
import com.skillmatic.jparepository.CourseenrolmentrawDao;
import com.skillmatic.jparepository.CourserawDao;
import com.skillmatic.jparepository.RoleDao;
import com.skillmatic.jparepository.UserDao;
import com.skillmatic.service.UserService;
import com.skillmatic.domain.Transaction;
import com.skillmatic.service.TransactionService;

import com.skillmatic.jparepository.TransactionDao;

import com.skillmatic.util.APIResponse;
import com.skillmatic.util.Helper;
import com.skillmatic.util.MailClient;



	@RestController
	@RequestMapping("/api")
	public class UserRestController {
		private static final Logger logger = LoggerFactory.getLogger(UserRestController.class);
		
		private static final String SALT = "salt"; // Salt should be protected carefully
		
		@Autowired
		UserService userService;
		
		@Autowired
		TransactionService transactionService;
		
		@Autowired
		 private Helper helper;
		
		@Autowired
	    private RoleDao roleDao;
		
		@Autowired
		private MailClient mailclient;
		
		@Autowired
		private CourserawDao courserawDao;
		
		@Autowired
		private CourseenrolmentrawDao courseenrolmentrawDao;
		
		@Autowired
		private CategoryDao categoryDao;
		
		@Autowired
		private TransactionDao transactionDao;
		
		@Autowired
		private UserDao userDao;
		
		
		@GetMapping(value="/getusers/all",
				produces=MediaType.APPLICATION_JSON_VALUE)

		public ResponseEntity<Iterable<User>> findall(){
			Iterable<User> users = userService.findUserList();
			return new ResponseEntity<Iterable<User>>(users,HttpStatus.OK);
		}
		
		@RequestMapping(
	            value = "/profile/{username}",
	            method = RequestMethod.GET,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
		public ResponseEntity<User> findUser(@PathVariable("username") String username) {
	        User user = userService.findByUsername(username);
	        return new ResponseEntity<User>(user,HttpStatus.OK);	    
		}
		
		@GetMapping(value="/refresh/courses",
				produces=MediaType.APPLICATION_JSON_VALUE)

		public ResponseEntity refreshCourses(){
			 userDao.batchdatamove();
			return new ResponseEntity("",HttpStatus.OK);
		}
		
		@GetMapping(value="/autologin/{userid}",
				produces=MediaType.APPLICATION_JSON_VALUE)

		public ResponseEntity autologinbyuserid(@PathVariable("userid") String userid){
			
			RestTemplate restTemplate = new RestTemplate();
        	String url = "https://skillmaticindia.talentlms.com/api/v1/userlogin/";
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", "application/json");
			headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
			String authurl = "";
			
			try {
				
				User user = userDao.findByUserId(Long.valueOf(userid));
				
				System.out.println("Assert that name > " + user.getUsername());
				
		    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		       map.add("login", user.getUsername());
		       map.add("password", user.getUsername()+"myPass");
		       
			
		    HttpEntity<MultiValueMap<String, String>> entity = new  HttpEntity<MultiValueMap<String, String>>(map, headers);
		    ResponseEntity<String> resultfromLMS = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
		    
		    System.out.println("Assert that > " + resultfromLMS.getStatusCode());
		    
		    
		    logger.debug("get test from LMS >> " + resultfromLMS );
		    
		    
		    
		    
		    	JSONParser parser = new JSONParser();
			    
			    JSONObject jobj= (JSONObject)parser.parse(resultfromLMS.getBody());
			    
			    authurl = (String)jobj.get("login_key");
			    
		    } catch (Exception e) {
		    	System.out.println("Exception ?>> " + e.getMessage());
		    	 return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, authurl), HttpStatus.OK);	
		    }
		    
			
		    return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, authurl), HttpStatus.OK);	
		}
		
		@GetMapping(value="/autologin/{username}/{password}",
				produces=MediaType.APPLICATION_JSON_VALUE)

		public ResponseEntity autologin(@PathVariable("username") String username, @PathVariable("password") String password){
			
			RestTemplate restTemplate = new RestTemplate();
        	String url = "https://skillmaticindia.talentlms.com/api/v1/userlogin";
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", "application/json");
			headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
			String authurl = "";
			
			try {
		    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		       map.add("login", username);
		       map.add("password", password);
		       
			
		    HttpEntity<MultiValueMap<String, String>> entity = new  HttpEntity<MultiValueMap<String, String>>(map, headers);
		    ResponseEntity<String> resultfromLMS = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
		    
		    System.out.println("Assert that > " + resultfromLMS.getStatusCode());
		    
		    
		    logger.debug("get test from LMS >> " + resultfromLMS );
		    
		    
		    
		    
		    	JSONParser parser = new JSONParser();
			    
			    JSONObject jobj= (JSONObject)parser.parse(resultfromLMS.getBody());
			    
			    authurl = (String)jobj.get("login_key");
			    
		    } catch (Exception e) {
		    	System.out.println("Exception ?>> " + e.getMessage());
		    	 return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, authurl), HttpStatus.OK);	
		    }
		    
			
		    return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, authurl), HttpStatus.OK);	
		}
		
		@RequestMapping(
	            value = "/getuserbyid/{userid}",
	            method = RequestMethod.GET,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
		public ResponseEntity<User> findUserById(@PathVariable("userid") String userid) {
	        User user = userService.findUserById(Long.valueOf(userid));
	        return new ResponseEntity<User>(user,HttpStatus.OK);	    
		}
		

		@RequestMapping(
	            value = "/profile/id/{userid}",
	            method = RequestMethod.GET,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
		public ResponseEntity<User> getProfilebyUserId(@PathVariable("userid") String userid) {
	        User user = userService.findUserById(Long.parseLong(userid));
	        return new ResponseEntity<User>(user,HttpStatus.OK);	    
		}
	

		@RequestMapping(
	            value = "/useractivation/{confirmtoken}",
	            method = RequestMethod.POST,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
		public ResponseEntity userActivation(@PathVariable("confirmtoken") String confirmtoken) {
			System.out.println("HHHH >><< " + confirmtoken);
	        int rowcount = userService.activateUser(confirmtoken);
	        
	        if(rowcount > 0) {
	        	return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, ""), HttpStatus.OK);	
	        } else {
	        	return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, ""), HttpStatus.OK);	
	        }
	        
	           
		}
		
		
		
		
		@RequestMapping(
	            value = "/updatepassword/{confirmtoken}/{password}",
	            method = RequestMethod.POST,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
		public ResponseEntity updatePassword(@PathVariable("confirmtoken") String confirmtoken, @PathVariable("password") String password) {
			System.out.println("updatepassword >><< " + confirmtoken);
			System.out.println("password >><< " + password);
			
			int rowcount = 0;
			
			try {
				rowcount = userService.resetpassword(confirmtoken, password);	
			} catch(Exception e) {
				logger.debug("Error in update password " + e.getMessage());
			}
	        
	        
	        if(rowcount > 0) {
	        	return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, ""), HttpStatus.OK);	
	        } else {
	        	return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, ""), HttpStatus.OK);	
	        }
	        
	           
		}
		 

		@RequestMapping(
	            value = "/changepassword/{password}/{userid}",
	            method = RequestMethod.POST,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
		public ResponseEntity changepassword(@PathVariable("password") String password, @PathVariable("userid") String userid) {
			System.out.println("change pass >> " + password);
			System.out.println("change userid >> " + userid);
			
	        int rowcount = userService.updatePassword(password, userid);
	        
	        if(rowcount > 0) {
	        	return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, ""), HttpStatus.OK);	
	        } else {
	        	return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, ""), HttpStatus.OK);	
	        }
	        
	           
		}
		
		
		
		
		
		@RequestMapping(
	            value = "/emailsignup",
	            method = RequestMethod.POST,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
			public ResponseEntity signup( @RequestBody String stringToParse, BindingResult bindingResult ) {
			
			logger.debug("kkkkkkkkkkkkkkkkkkkk:::::::;  " + stringToParse);
		        
			JSONObject result = getGeneric(stringToParse);
			
            String email = (String)result.get("email");
            String password = (String)result.get("password");
            String fullname = (String)result.get("fullname");
            String source = (String)result.get("source");
            
			
			 if (email != null && userService.checkProfileEmailExists(email)) {
				 logger.debug("Email Already Exists..");
				 return new ResponseEntity<>(new APIResponse("DP-EMAIL", HttpStatus.OK, ""), HttpStatus.OK);   
			 }
			 
			 if (fullname!= null && userService.checkUsernameExists(fullname)) {
				 logger.debug("username Already Exists..");
				 return new ResponseEntity<>(new APIResponse("DP-USERNAME", HttpStatus.OK, ""), HttpStatus.OK);   
			 }
			 
			 Set<UserRole> userRoles = new HashSet<>();
			 User user = new User();

			 user.setEmail(email);
			 user.setEnabled(false);
			 user.setFirstName(fullname);
			 user.setLastName("");
			
			 user.setPassword(password);
			 user.setPhone("919999999999");
	
			 user.setProfileimgurl("");
			// user.setSocialid("");
			 user.setSource(source);
			 
		//	 user.setUsername(fullname.replaceAll("\\s+",""));

			 
             userRoles.add(new UserRole(user, roleDao.findByName("ROLE_USER")));

            userService.createUser(user, userRoles);
        	
         
        		return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, ""), HttpStatus.OK);	 
			
		}
		
		
		
		@RequestMapping(
	            value = "/emailsignin",
	            method = RequestMethod.POST,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
		
		public ResponseEntity signin( @RequestBody String stringToParse, BindingResult bindingResult ) {
			
			logger.debug("sign @input json>>" + stringToParse);
		        
			JSONObject result = getGeneric(stringToParse);
			
            String emailid = (String)result.get("emailid");
            String password = (String)result.get("password");
            
            String source = (String)result.get("source");
            
            
            
			 if (!userService.checkProfileEmailExists(emailid)) {
				 return new ResponseEntity<>(new APIResponse("User not Found",HttpStatus.OK,""), HttpStatus.OK);	 
				 
			 } else {
				 User user = userService.getUserByEmail(emailid);
				 
				 
				 logger.debug("USER.. DETAILS >> "+ user.toString());
				 
				 BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(12, new SecureRandom(SALT.getBytes()));
				 
				if (!passwordEncoder.matches(password, user.getPassword())) {
					logger.debug("ERROR");
					return new ResponseEntity<>(new APIResponse("Password did not Match",HttpStatus.OK,""), HttpStatus.OK);	 
				} else {
					logger.debug("SSSSS"+ user.getUsername());
					 ObjectMapper mapper = new ObjectMapper();
					 String json = "";
				        try {
				             json = mapper.writeValueAsString(user);
				            logger.debug("JSON = " + json);
				        } catch (JsonProcessingException e) {
				            e.printStackTrace();
				        }
					return new ResponseEntity<>(new APIResponse("success",HttpStatus.OK, json ), HttpStatus.OK);	 
				}
				 
			 }			
		}
		
		
		
	
		
		@RequestMapping(
	            value = "/recoverpass",
	            method = RequestMethod.POST,
	            produces = MediaType.APPLICATION_JSON_VALUE)
		
		
		public ResponseEntity recoverpass( @RequestBody String email, BindingResult bindingResult ) {
	//	public ResponseEntity recoverpass( @PathVariable("email") String email ) {
			User user = null;
			
			logger.debug("recoverpass @input JSON >>   " + email);
		     try {    
			if (email != null && !userService.checkEmailExists(email)) {
				 logger.debug("Email Does not Exists..");
				 return new ResponseEntity<>(new APIResponse("EMAIL-NF", HttpStatus.OK, ""), HttpStatus.OK);   
			 }
			
			
			user = userService.getUserByEmail(email);
			
			String confirmtokenid = helper.getRandomAlphaNumericString(20);
			 user.setConfirmtoken(confirmtokenid);
			int confirmtoken =  userService.updateConfirmToken(user.getUserId(), confirmtokenid);
			
			 String emailbody = resetEmailFormat(user.getFirstName(), user.getLastName(), "resetpassword", confirmtokenid);
	            
	            if(confirmtoken > 0) {
	            	mailclient.sendResetPassword(email, emailbody);
	            }
			
			
		     } catch (Exception e) {
		    	 	
		    	 	logger.debug("Error while processing recover password "+ e.getMessage());
		    		return new ResponseEntity<>(new APIResponse("ERROR", HttpStatus.OK, ""), HttpStatus.OK);
		     }
			
			return new ResponseEntity<>(new APIResponse("SUCCESS", HttpStatus.OK, user.getUsername()), HttpStatus.OK);
						
		}
		
		
		   public JSONObject getGeneric( String stringToParse){
		        JSONParser parser = new JSONParser();
		        JSONObject result = null;
		        JSONArray jsonArray = null;
		        String email = null;
		        
		        try {
		        	jsonArray = (JSONArray) parser.parse(stringToParse);
		            
		            result = (JSONObject)jsonArray.get(0);
		            
		        } catch (ParseException e) {
		            e.printStackTrace();
		        }
		        return result;
		    } 
		   
					
			
//		   @SuppressWarnings("unchecked")
//
//			@GetMapping(value="/getcourses",
//
//					produces=MediaType.APPLICATION_JSON_VALUE)
//
//			public ResponseEntity getCourses(){
//
//				RestTemplate restTemplate = new RestTemplate();
//				String url = "https://caerus.talentlms.com/api/v1/courses/";
//				HttpHeaders headers = new HttpHeaders();
//				headers.set("Accept", "application/json");
//				headers.add("Authorization", "Basic NTFOWlJhS2JCeXdyZDlSNHNlNVZpeTVmYzVTaUtyOg==");
//			    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
//			    ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//			    JSONParser parser = new JSONParser();
//		        JSONArray jsonArrays = null;
//	        	
//		        courserawDao.deleteAll();
//       		courseenrolmentrawDao.deleteAll();
//       		
//		        try {
//		        	jsonArrays = (JSONArray) parser.parse(result.getBody());
//		        	jsonArrays.forEach(item-> {
//		        	Courseraw tempcourses = new Courseraw();
//		        	tempcourses.setId(Long.valueOf((String)((JSONObject)item).get("id")));
//		        	tempcourses.setName((String)((JSONObject)item).get("name"));
//		        	tempcourses.setCode((String)((JSONObject)item).get("code"));
//		        	tempcourses.setCategory_id((String)((JSONObject)item).get("category_id"));
//		        	tempcourses.setDescription((String)((JSONObject)item).get("description"));
//		        	tempcourses.setPrice((String)((JSONObject)item).get("price"));
//		        	tempcourses.setStatus((String)((JSONObject)item).get("status"));
//		        	tempcourses.setCreation_date(Helper.dateConversionStr2DtDDMMYYYYHHMISS((String)((JSONObject)item).get("creation_date")));
//		        	tempcourses.setLast_update_on(Helper.dateConversionStr2DtDDMMYYYYHHMISS((String)((JSONObject)item).get("last_update_on")));
//		        	tempcourses.setCreator_id(Long.valueOf((String)((JSONObject)item).get("creator_id")));
//		        	tempcourses.setHide_from_catalog((String)((JSONObject)item).get("hide_from_catalog"));
//		        	tempcourses.setTime_limit((String)((JSONObject)item).get("time_limit"));
//		        	tempcourses.setLevel((String)((JSONObject)item).get("level"));
//		        	tempcourses.setShared((String)((JSONObject)item).get("shared"));
//		        	tempcourses.setShared_url((String)((JSONObject)item).get("shared_url"));
//		        	tempcourses.setAvatar((String)((JSONObject)item).get("avatar"));
//		        	tempcourses.setBig_avatar((String)((JSONObject)item).get("big_avatar"));
//		        	tempcourses.setCertification((String)((JSONObject)item).get("certification"));
//		        	tempcourses.setCertification_duration((String)((JSONObject)item).get("certification_duration"));
//		        	
//		        	courserawDao.save(tempcourses);
//
//		        	
//		        	{
//		        	
//
//		    			RestTemplate restTemplateen = new RestTemplate();
//						String urlen = "https://caerus.talentlms.com/api/v1/courses/id:"+Long.valueOf((String)((JSONObject)item).get("id"));
//						System.out.println(" ** ENRL ** " + urlen);
//						HttpHeaders headersen = new HttpHeaders();
//						headersen.set("Accept", "application/json");
//						headersen.add("Authorization", "Basic NTFOWlJhS2JCeXdyZDlSNHNlNVZpeTVmYzVTaUtyOg==");
//					    HttpEntity<String> entityen = new HttpEntity<String>("parameters", headersen);
//					    ResponseEntity<String> resulten = restTemplateen.exchange(urlen, HttpMethod.GET, entityen, String.class);
//					    JSONParser parseren = new JSONParser();
//				    	try {
//				    		
//				    		JSONObject item1 = (JSONObject) parseren.parse(resulten.getBody());
//				    		
//
//				    			JSONArray usersArray = ((JSONArray)((JSONObject)item1).get("users"));
//				    		
//				    			usersArray.forEach(user-> System.out.println("AAA"+ user));
//				    			usersArray.forEach(user-> {
//				    				Courseenrolmentraw tempcoursesenrl = new Courseenrolmentraw();
//				    							tempcoursesenrl.setCourseid(Long.valueOf((String)((JSONObject)item).get("id")));
//				    							tempcoursesenrl.setUserid(Long.valueOf((String)((JSONObject)user).get("id"))); 
//				    							tempcoursesenrl.setRole((String)((JSONObject)user).get("role"));
//				    							tempcoursesenrl.setTotal_time((String)((JSONObject)user).get("total_time"));
//				    							tempcoursesenrl.setCompletion_percentage(Long.valueOf((String)((JSONObject)user).get("completion_percentage"))); 
//				    							tempcoursesenrl.setName((String)((JSONObject)user).get("name"));
//				    							tempcoursesenrl.setEnrolled_on(Helper.dateConversionStr2DtDDMMYYYYHHMISS((String)((JSONObject)user).get("enrolled_on")));
//				    							courseenrolmentrawDao.save(tempcoursesenrl);
//
//				    			});
//				    			
//				    		
//				    		
//				    		
//				    		} catch (ParseException e) {
//				    		
//				    		e.printStackTrace();
//				    		
//				    		}
//				    			
//				        
//		        	}
//		        	
//		        	System.out.println("UUUU>> " + tempcourses.toString());
//
//		        	});
//		           
//
//		        } catch (ParseException e) {
//
//		            e.printStackTrace();
//		        }
//		        
//		        {
//		        	RestTemplate restTemplatect = new RestTemplate();
//					String urlct = "https://caerus.talentlms.com/api/v1/categories/";
//					HttpHeaders headersct = new HttpHeaders();
//					headersct.set("Accept", "application/json");
//					headersct.add("Authorization", "Basic NTFOWlJhS2JCeXdyZDlSNHNlNVZpeTVmYzVTaUtyOg==");
//				    HttpEntity<String> entityct = new HttpEntity<String>("parameters", headers);
//				    ResponseEntity<String> resultct = restTemplatect.exchange(urlct, HttpMethod.GET, entityct, String.class);
//				    JSONParser parserct = new JSONParser();
//			        JSONArray jsonArraysct = null;
//		        	
//			        categoryDao.deleteAll();
//	        		
//			        try {
//			         	jsonArraysct = (JSONArray) parser.parse(resultct.getBody());
//			        
//			        	jsonArraysct.forEach(itemct-> {
//			        	Category tempcategory = new Category();
//			        	tempcategory.setId(Long.valueOf((String)((JSONObject)itemct).get("id")));
//			        	System.out.println("after ID");
//			        	tempcategory.setName((String)((JSONObject)itemct).get("name"));
//			        	System.out.println("after Name");
//			        	tempcategory.setPrice((String)((JSONObject)itemct).get("price"));
//			        	tempcategory.setParent_category_id(Optional.ofNullable((String)((JSONObject)itemct).get("parent_category_id")).orElse(""));
//			        	
//			        //	System.out.println("Parent ID" + (String)((JSONObject)itemct).get("parent_category_id"));
//			        	
//			        	String parentID =  Optional.ofNullable((String)((JSONObject)itemct).get("parent_category_id")).orElse("");
//			         	System.out.println(" Assigned " + parentID + " --- CHECK --- ") ;
//			         	
//			        	categoryDao.save(tempcategory);
//
//			        	});
//		    			
//			    		
//			    		
//			    		
//		    		} catch (ParseException e) {
//		    		
//		    		e.printStackTrace();
//		    		
//		    		}
//		        }
//
//
//			    logger.debug("JSON VALUE >>  " + result);
//			    logger.debug("get courses from LMS >> " + result );
//
//				return new ResponseEntity("",HttpStatus.OK);
//
//			}  
			
//			@GetMapping(value="/getcoursesbyid/{id}",
//					produces=MediaType.APPLICATION_JSON_VALUE)
//			public ResponseEntity getCoursesById(@PathVariable("id") String id){
//				RestTemplate restTemplate = new RestTemplate();
//				
//				System.out.println("ABCD>>  " + id);
//				
//				String url = "https://caerus.talentlms.com/api/v1/courses/id:"+id;
//				HttpHeaders headers = new HttpHeaders();
//				headers.set("Accept", "application/json");
//				headers.add("Authorization", "Basic NTFOWlJhS2JCeXdyZDlSNHNlNVZpeTVmYzVTaUtyOg==");
//			    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
//			    ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//			    
//			   
//			   
//			    JSONParser parser = new JSONParser();
//		        JSONArray jsonArrays = null;
//				   
//		        try {
//		        	JSONObject item = (JSONObject) parser.parse(result.getBody());
//		        		System.out.println(((JSONObject)item).get("id"));
//		        		System.out.println(((JSONObject)item).get("price"));
//		        		
//		        		JSONArray usersArray = ((JSONArray)((JSONObject)item).get("users"));
//		        		
//		        		usersArray.forEach(user-> System.out.println("AAA"+ user));
//		        		
//		            
//		        } catch (ParseException e) {
//		            e.printStackTrace();
//		        }
//				return new ResponseEntity("",HttpStatus.OK);
//				
//			}			

			

			@RequestMapping(
		            value = "/register",
		            method = RequestMethod.POST,
		            produces = MediaType.APPLICATION_JSON_VALUE)
			
				public ResponseEntity register( @RequestBody String stringToParse, BindingResult bindingResult ) {
				
				logger.debug("kkkkkkkkkkkkkkkkkkkk:::::::;  " + stringToParse);
				logger.debug("in register >> " + stringToParse);
				
			        try {
				JSONObject result = getGeneric(stringToParse);
				
	            String email = (String)result.get("email");
	            String password = (String)result.get("password");
	            String firstname = (String)result.get("firstname");
	            String lastname = (String)result.get("lastname");
	            String phno = (String)result.get("phno");
	            String source = (String)result.get("source");
	            String profiletype = (String)result.get("profiletype");
	            
	            String universityname = (String)result.get("universityname");
	            String collegename = (String)result.get("collegename");
	            
	            String position = (String)result.get("position");
	            String universitysize = (String)result.get("universitysize");
	            
	            String company = (String)result.get("company");
	            
	            String organizationtype = (String)result.get("organizationtype");
	            String organizationsize = (String)result.get("organizationsize");
	            

	            String institution = (String)result.get("institution");
	            
	            String jobrole = (String)result.get("jobrole");
	            
	            
	            String country = (String)result.get("country");
	            String city = (String)result.get("city");
	            String zipcode = (String)result.get("zip");
	            
				
				 if (email != null && userService.checkEmailExists(email)) {
					 logger.debug("Email Already Exists..");
					 return new ResponseEntity<>(new APIResponse("DP-EMAIL", HttpStatus.OK, ""), HttpStatus.OK);   
				 }
				 
				
				 
				 Set<UserRole> userRoles = new HashSet<>();
				 User user = new User();

				 user.setEnabled(false);
				 user.setFirstName(firstname);
				 user.setLastName(lastname);
				 
				 user.setPassword(password);
				 user.setPhone(phno);
				 user.setEmail(email);
				 
				 user.setProfileimgurl("");

				 user.setSource(source);
				 user.setProfiletype(profiletype);
				 
				 user.setUniversityname(universityname);
				 user.setCollegename(collegename);
				 user.setPosition(position);
				 user.setUniversitysize(universitysize);
				 
				 user.setInstitution(institution);
				 
				 user.setCompany(company);
				 user.setOrgsize(organizationsize);
				 user.setOrgtype(organizationtype);
				 user.setJobrole(jobrole);
				 
				 user.setZipcode(zipcode);
				 user.setCity(city);
				 user.setCountry(country);
				 
				 String confirmtokenid = helper.getRandomAlphaNumericString(20);
				 user.setConfirmtoken(confirmtokenid);
				 
				 String uniqueuname = "";

				 if(userService.checkUsernameExists(firstname.replaceAll("\\s+",""))) {
    				 String origuname = firstname.replaceAll("\\s+","");
    				 uniqueuname = origuname + helper.randomfourdigitnumber();
    				 
    			 } else {
    				 uniqueuname = firstname.replaceAll("\\s+","");
    			 }

				 user.setUsername(uniqueuname);
				 
				 
				 if(profiletype!= null && profiletype.equalsIgnoreCase("Seeker")) {
					 userRoles.add(new UserRole(user, roleDao.findByName("ROLE_SEEKER")));	 
				 } else if(profiletype!= null && profiletype.equalsIgnoreCase("Provider")) {
					 userRoles.add(new UserRole(user, roleDao.findByName("ROLE_PROVIDER")));
				 } else if(profiletype!= null && profiletype.equalsIgnoreCase("Expert")) {
					 userRoles.add(new UserRole(user, roleDao.findByName("ROLE_EXPERT")));
				 } else if(profiletype!= null && profiletype.equalsIgnoreCase("Industry")) {
					 userRoles.add(new UserRole(user, roleDao.findByName("ROLE_INDUSTRY")));
				 } else if(profiletype!= null && profiletype.equalsIgnoreCase("University")) {
					 userRoles.add(new UserRole(user, roleDao.findByName("ROLE_UNIVERSITY")));
				 }
				 
	             

	            
	           User newuser =  userService.createUser(user, userRoles);
	           
	           String emailbody = formatActivationEmail(firstname, lastname, "useractivation", confirmtokenid);
	            
	            if(newuser != null) {
	            	mailclient.sendActivateRegistration(email, emailbody);
	            }
	            
	            JSONObject obj = new JSONObject();
	            
	            if(profiletype.equalsIgnoreCase("Seeker")) {
	            	
		            obj.put("first_name", firstname);
		            obj.put("last_name", lastname);
		            obj.put("email", email);
		            obj.put("login", uniqueuname);
		            obj.put("password", uniqueuname+"myPass");
	            	obj.put("user_type", "Learner-Type");
	            }
	            else {
		            obj.put("first_name", firstname);
		            obj.put("last_name", lastname);
		            obj.put("email", email);
		            obj.put("login", uniqueuname);
		            obj.put("password", uniqueuname+"myPass");
	            	obj.put("user_type", "Trainer-Type");
	            }
	            
	            	

	            System.out.println("JSON >>>>><<<< " + obj.toJSONString());
	            
	        	RestTemplate restTemplate = new RestTemplate();
	        	String url = "https://skillmaticindia.talentlms.com/api/v1/usersignup/";
				HttpHeaders headers = new HttpHeaders();
				headers.set("Accept", "application/json");
				headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
			    HttpEntity<String> entity = new HttpEntity<String>(obj.toJSONString(), headers);
			    ResponseEntity<String> resultfromLMS = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			    
			    
			   // logger.debug("JSON VALUE >>  " + result);
			    logger.debug("get courses from LMS >> " + resultfromLMS );
			    
			    
	          
			        } catch (Exception e) {
			        	logger.debug("Error in register >> "+ e.getMessage());
			            
			        }
	         
	        		return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, ""), HttpStatus.OK);	 
				
			}
			
			

			@RequestMapping(
		            value = "/edit/profile",
		            method = RequestMethod.POST,
		            produces = MediaType.APPLICATION_JSON_VALUE)
			
				public ResponseEntity editprofile( @RequestBody String stringToParse, BindingResult bindingResult ) {
			
				logger.debug("in register >> " + stringToParse);
				
			        try {
				JSONObject result = getGeneric(stringToParse);
				
	            String email = (String)result.get("email");
	            String password = (String)result.get("password");
	            String firstname = (String)result.get("firstname");
	            String lastname = (String)result.get("lastname");
	            String phno = (String)result.get("phno");
	            String source = (String)result.get("source");
	            String profiletype = (String)result.get("profiletype");
	            
	            String country = Optional.ofNullable((String)result.get("country")).orElse(""); 
	            		
	            String city = (String)result.get("city");
	            String zipcode = (String)result.get("zip");
	            
	            String state = (String)result.get("state");
	            String citizenship = (String)result.get("citizenship");
	            String aadhar = (String)result.get("aadhar");
	            String pan = (String)result.get("pan");
	            String smgoogle = (String)result.get("smgoogle");
	            String smfacebook = (String)result.get("smfacebook");
	            String smlinkedin = (String)result.get("smlinkedin");
	            String smtwitter = (String)result.get("smtwitter");
	            String smyoutube = (String)result.get("smyoutube");
	            String website = (String)result.get("website");
	        
	            String gender = Optional.ofNullable((String)result.get("sex")).orElse(""); 
	            		
	            String dob =   Optional.ofNullable((String)result.get("dob")).orElse("");
	            
	            
	            
	            String nationality =   Optional.ofNullable((String)result.get("nationality")).orElse("");
	          
				
				String userid = 	Optional.ofNullable((String)result.get("userid")).orElse("");
				 
			System.out.println("1" + nationality);
				
				 User user = new User();
				 user.setUserId(Long.parseLong(userid));
				 System.out.println("2");
				 user.setEnabled(false);
				 user.setFirstName(firstname);
				 user.setLastName(lastname);
				 
				 user.setPassword(password);
				 user.setPhone(phno);
				 user.setEmail(email);
				 
				 user.setProfileimgurl("");

				 user.setSource(source);
				 user.setProfiletype(profiletype);
				 
			
				 user.setZipcode(zipcode);
				 user.setCity(city);
				 user.setCountry(country);
				 
				 user.setState(state);
				 user.setAadhar(aadhar);
				 user.setCitizenship(citizenship);
				 user.setPan(pan);
				 user.setSmfacebook(smfacebook);
				 user.setSmgoogle(smgoogle);
				 user.setSmlinkedin(smlinkedin);
				 
				 user.setSmtwitter(smtwitter);
				 user.setSmyoutube(smyoutube);
				 user.setWebsite(website);
				
			     user.setDob(helper.dateConversionDDMMYYYYslash(dob));
			     user.setGender(gender);
			     user.setNationality(nationality);
			     System.out.println("7");
				 user.setProfiletype(profiletype);
				 
				 logger.debug("Profile Update !! >> "+ user.toString());
				 
				int rowcount =  userService.updateUser(user);
				 logger.debug("Row cout " + rowcount);
	          
			        } catch (Exception e) {
			        	logger.debug("Profile Update >> "+ e.getMessage());
			            
			        }
	         
	        		return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, ""), HttpStatus.OK);	 
				
			}
			
			
			@RequestMapping(
		            value = "/edit/profile-detail",
		            method = RequestMethod.POST,
		            produces = MediaType.APPLICATION_JSON_VALUE)
			
				public ResponseEntity editprofileDetail(@RequestBody String stringToParse, BindingResult bindingResult ) {
			
				logger.debug("in register >> " + stringToParse);
				
			        try {
				JSONObject result = getGeneric(stringToParse);
				
				String firstname = (String) result.get("firstname");
				String lastname = (String) result.get("lastname");
				String city = (String) result.get("city");
				String phone = (String) result.get("phno");
				String gender = (String) result.get("sex");
				String country = (String) result.get("country");
				String state = (String) result.get("state");
				String aadhar = (String) result.get("aadhar");
				String pan = (String) result.get("pan");
				String smgoogle = (String) result.get("smgoogle");
				String smfacebook = (String) result.get("smfacebook");
				String smtwitter = (String) result.get("smtwitter");
				String website = (String) result.get("website");
				
				String smlinkedin = (String) result.get("smlinkedin");
				String smyoutube = (String) result.get("smyoutube");
				

				
				
				String secondaryeducation = (String)result.get("secondaryeducation");
				String secedupercent = (String)result.get("secedupercent");
		        String secedupassoutyr = (String)result.get("secedupassoutyr");
				
		        String puedu = (String)result.get("puedu");
	            String puedupercent = (String)result.get("puedupercent");
	            String puedupassoutyr = (String)result.get("puedupassoutyr");
	            
	            String ugedu = (String)result.get("ugedu");
	            String ugspeciality = (String)result.get("ugspeciality");
	            String ugedupercent = (String)result.get("ugedupercent");
	            String ugedupassoutyr = (String)result.get("ugedupassoutyr");
	            
	            String pgedu = (String)result.get("pgedu");
	            String pgedupercent = (String)result.get("pgedupercent");
	            String pgedupassoutyr = (String)result.get("pgedupassoutyr");
	            
	            String experience = (String)result.get("experience");
	            String industry = (String)result.get("industry");
	            
	            String subindustry = (String)result.get("subindustry");
	            String institution = (String)result.get("institution");
	            
	            String currentorganization = (String)result.get("currentorg");
	            
	            String designation = (String)result.get("designation");
	            
	            String keyskills = (String)result.get("keyskills");
	            
				String ctc = Optional.ofNullable((String)result.get("ctc")).orElse("");  
						
				String userid = 	Optional.ofNullable((String)result.get("userid")).orElse("");
				 
				
				 User user = new User();
				 user.setUserId(Long.parseLong(userid));
			
			
				 user.setProfileimgurl("");
				
			
				 user.setSecondaryeducation(secondaryeducation);
				 System.out.println("4");
				
				 
				 user.setFirstName(firstname);
				 user.setLastName(lastname);
				 user.setCity(city);
				 user.setPhone(phone);
				 user.setGender(gender);
				 user.setCountry(country);
				 user.setState(state);
				 user.setAadhar(aadhar);
				 user.setPan(pan);
				 user.setSmgoogle(smgoogle);
				 user.setSmfacebook(smfacebook);
				 user.setSmtwitter(smtwitter);
				 user.setWebsite(website);
				 user.setSmlinkedin(smlinkedin);
				 user.setSmyoutube(smyoutube);
				 
				 user.setSecedupercent(secedupercent);
				 user.setSecedupassoutyr(secedupassoutyr);
				 user.setPuedu(puedu);
				 
				 user.setPuedupercent(puedupercent);
			     user.setPuedupassoutyr(puedupassoutyr);
			     user.setUgedu(ugedu);
			     user.setUgspeciality(ugspeciality);
			     user.setUgedupercent(ugedupercent);
			     user.setUgedupassoutyr(ugedupassoutyr);
			     user.setPgedu(pgedu);
			     user.setPgedupercent(pgedupercent);
			    
			     user.setPgedupassoutyr(pgedupassoutyr);
			     System.out.println("5");
			     user.setExperience(experience);
			     user.setIndustry(industry);
			     user.setInstitution(institution);
			     user.setSubindustry(subindustry);
			     user.setCurrentorg(currentorganization);
			     user.setDesignation(designation);
			     user.setCtc(ctc);
			     user.setKeyskills(keyskills);
				 
				 logger.debug("Profile DEtail Update !! >> "+ user.toString());
				 
				int rowcount =  userService.updateUserDetails(user);
				 logger.debug("Row cout " + rowcount);
	          
			        } catch (Exception e) {
			        	logger.debug("Profile Detail Update >> "+ e.getMessage());
			        	return new ResponseEntity<>(new APIResponse("FAILURE",HttpStatus.OK, ""), HttpStatus.OK);	
			            
			        }
	         
	        		return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, ""), HttpStatus.OK);	 
				
			}	
			 
		    @Transactional
		    @RequestMapping(
		            value = "/profilepicupload",
		            method = RequestMethod.POST,
		            produces = MediaType.APPLICATION_JSON_VALUE)
			
			public ResponseEntity profilepicupload( @RequestParam MultipartFile image, @RequestHeader(value="Authorization") String authid, @RequestParam String userid) {

				String originalfilenamewithext = image.getOriginalFilename();
							
				String id = userid.toString();
				
				System.out.println("ANCDMM >> ");
				
			    try {
			    
			    if(!image.isEmpty()) {

			    	//upload original image
			    	File profileOriginalImageFile = new File(helper.getProfileUploadFolder() + id + System.getProperty("file.separator") +"original");
			    	
			    	profileOriginalImageFile.mkdirs();
			    	
			    	Files.write(Paths.get((new File(helper.getProfileUploadFolder() + System.getProperty("file.separator") + id).toString() + System.getProperty("file.separator") +"original" + System.getProperty("file.separator") + image.getOriginalFilename())), image.getBytes());
			    	
			    	File profileCoverImageFile = new File(helper.getProfileUploadFolder() + id + System.getProperty("file.separator") +"coverimage");
			    	
			    	profileCoverImageFile.mkdirs();
			    	
			    	helper.deleteFilesinDir(profileCoverImageFile);
			    	
			    	Files.write(Paths.get((new File(helper.getProfileUploadFolder() + System.getProperty("file.separator") + id).toString() + System.getProperty("file.separator") +"coverimage" + System.getProperty("file.separator") + image.getOriginalFilename())), image.getBytes());	    	
			    	
			 		//setting image path to store in db
			 		String uploadProfileImagePath = System.getProperty("file.separator") + "img" + System.getProperty("file.separator") + "profiles" + System.getProperty("file.separator") + id + System.getProperty("file.separator") +"coverimage" +  System.getProperty("file.separator") + image.getOriginalFilename();
			 	
			 		int count = userService.updateProfileImgUrl(uploadProfileImagePath, Long.parseLong(id));
			 		
			 		
			 	}
			    } catch (Exception e) {
			    	e.printStackTrace();
			    		logger.error("Error ..in userrest categorypicupload .." + e.getMessage());
			    		return new ResponseEntity<>(new APIResponse("Max File Size",HttpStatus.OK,""),HttpStatus.OK);	 
			    }
			   	    
		        return new ResponseEntity<>(new APIResponse("success",HttpStatus.OK,""), HttpStatus.OK);	 
			}
		    
			
		    
			 
		    @Transactional
		    @RequestMapping(
		            value = "/resumeupload",
		            method = RequestMethod.POST,
		            produces = MediaType.APPLICATION_JSON_VALUE)
			
			public ResponseEntity resumeupload( @RequestParam MultipartFile resume, @RequestHeader(value="Authorization") String authid, @RequestParam String userid) {

				String originalfilenamewithext = resume.getOriginalFilename();
							
				String id = userid.toString();
				
				System.out.println("resumeupload >> " + originalfilenamewithext);
				
			    try {
			    
			    if(!resume.isEmpty()) {

			    	//upload original image
			    	File profileOriginalFile = new File(helper.getResumeUploadFolder() + id );
			    	
			    	profileOriginalFile.mkdirs();
			    	helper.deleteFilesinDir(profileOriginalFile);
			    	
			    	Files.write(Paths.get((new File(helper.getResumeUploadFolder() + System.getProperty("file.separator") + id).toString()  + System.getProperty("file.separator") + resume.getOriginalFilename())), resume.getBytes());
			    	
			    	
			 		//setting image path to store in db
			 		String uploadResumePath = System.getProperty("file.separator") + "resume" + System.getProperty("file.separator") + "profiles" + System.getProperty("file.separator") + id + System.getProperty("file.separator") + resume.getOriginalFilename();
			 	
			 		int count = userService.updateResumeUrl(uploadResumePath, Long.parseLong(id));
			 		
			 		
			 	}
			    } catch (Exception e) {
			    	e.printStackTrace();
			    		logger.error("Error ..in userrest resumeupload .." + e.getMessage());
			    		return new ResponseEntity<>(new APIResponse("Max File Size",HttpStatus.OK,""),HttpStatus.OK);	 
			    }
			   	    
		        return new ResponseEntity<>(new APIResponse("success",HttpStatus.OK,""), HttpStatus.OK);	 
			}
		    
		  private String formatActivationEmail(String firstname, String lastname, String activationurl, String confirmtoken) {
				 
				  
				  StringBuffer stb = new StringBuffer();
				  stb.append("Dear <b>"+ StringUtils.capitalize(firstname) + " "+ StringUtils.capitalize(lastname) +  "</b>,");
				  stb.append("\n<br><br>");
				  stb.append("Thank you for being part of <b>caerus SkillmaTic platform.</b>");
				  stb.append("\n<br><br>");
		          stb.append("Click here to <a href= " + helper.getBaseURL() + "/" + activationurl + "/" + confirmtoken +"><b>Activate your Account</b></a>." );
		          stb.append("\n<br><br>");
		          stb.append("\nseeker registration-Email Verification-Help? Contact us on +91-7259109772.<br>");
		          stb.append("Thanks and Regards,");
		          stb.append("\n<br>");
		          stb.append("Skillmatic.");
		          return stb.toString();
						  
			  }
		  
		  private String resetEmailFormat(String firstname, String lastname, String activationurl, String confirmtoken) {
				 
			  
			  StringBuffer stb = new StringBuffer();
			  stb.append("Dear <b>"+ StringUtils.capitalize(firstname) + " "+ StringUtils.capitalize(lastname) +  "</b>,");
			  stb.append("\n<br><br>");
			  stb.append("Your Password reset request is Accepted. <b>caerus SkillmaTic platform.</b>");
			  stb.append("\n<br><br>");
	          stb.append("Click here to <a href= " + helper.getBaseURL() + "/" + activationurl + "/" + confirmtoken +"><b>Reset your Account</b></a>." );
	          stb.append("\n<br><br>");
	          stb.append("\nReset Password Email Verification-Help? Contact us on +91-7259109772.<br>");
	          stb.append("Thanks and Regards,");
	          stb.append("\n<br>");
	          stb.append("Skillmatic.");
	          return stb.toString();
					  
		  }
		  
		  

			
			@RequestMapping(
		            value = "/cardpayment",
		            method = RequestMethod.POST,
		            produces = MediaType.APPLICATION_JSON_VALUE)
			
				public ResponseEntity cardpayment( @RequestBody String stringToParse, BindingResult bindingResult ) {
				
				logger.debug("kkkkkkkkkkkkkkkkkkkk:::::::;  " + stringToParse);
			        
				JSONObject result = getGeneric(stringToParse);
				

				logger.debug("objjjjjjjjr res:::::::;  " + result);

				String username = (String)result.get("username");
				Number courseid = (Number)result.get("courseid");
								
				
				logger.debug("details:::::::;  " + username + courseid);
				
				
				
				RestTemplate restTemplate = new RestTemplate();
		    	String url = "https://skillmaticindia.talentlms.com/api/v1/users/";
				HttpHeaders headers = new HttpHeaders();
				headers.set("Accept", "application/json");
				headers.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
			
				String usrId = "";
				String first_name= "";
				String email= "";
				
				
				try {
			    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			       map.add("username", username);
			       
				
			    HttpEntity<MultiValueMap<String, String>> entity = new  HttpEntity<MultiValueMap<String, String>>(map, headers);
			    ResponseEntity<String> resultfromLMS = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			    
			    System.out.println("Assert that > " + resultfromLMS.getStatusCode());
			    
			    
			    logger.debug("get test from LMS >> " + resultfromLMS );
			    
			    
			    
			    
			    	JSONParser parser = new JSONParser();
			    	 
				    JSONObject jobj= (JSONObject)parser.parse(resultfromLMS.getBody());
				    
				    usrId = (String)jobj.get("id");
				    
				     email =(String)jobj.get("email");
				     first_name =(String)jobj.get("first_name");
				  // logger.debug ("Usercourses > " + usrId);
				    
				    
				    
			    } catch (Exception e) {
			    	System.out.println("Exception ?>> " + e.getMessage());
			    }
				
				
				RestTemplate restTemplate1 = new RestTemplate();
	        	String url1 = "https://skillmaticindia.talentlms.com/api/v1/courses/";
				HttpHeaders headers1 = new HttpHeaders();
				headers1.set("Accept", "application/json");
				headers1.add("Authorization", "Basic U3BxUDRCaFB4QTNTSXhhbGFReGdmUjdEcDM0ZkZSOg==");
				String Price = "";
				
				try {
				 MultiValueMap<String, Number> map1 = new LinkedMultiValueMap<String, Number>();
			       map1.add("id", courseid );
			       
				 HttpEntity<MultiValueMap<String, Number>> entity1 = new  HttpEntity<MultiValueMap<String, Number>>(map1, headers1);
				    ResponseEntity<String> resultfromLMS1 = restTemplate1.exchange(url1, HttpMethod.POST, entity1, String.class);
				    
				    System.out.println("Assert that > " + resultfromLMS1.getStatusCode());
				    

			    	JSONParser parser1 = new JSONParser();
				    
				    JSONObject jobj1= (JSONObject)parser1.parse(resultfromLMS1.getBody());
				    
				    Price = (String)jobj1.get("price");
				    Price = Price.replaceAll("[?,]","");
				    Price = Price.substring(1);
				
				    logger.debug ("PRICEEEEEE > " + Price);
				
				} catch (Exception e) {
			    	System.out.println("Exception ?>> " + e.getMessage());
			    }
			
				
		//		logger.debug("priceeeeeee:::::::;  " + price);
				  
	            
	            String merchantId = "162062";   
	   		// String accessCode = "AVDE75FA46AS29EDSA ";	// Put in the Access Code provided by CCAVENUES
	   		// String workingKey = "740D13236E93A210192C6829F24E8E04";    // Put in the Working Key provided by CCAVENUES	
	           String workingKey = "5AA181C8FFF4642D9CF0265E0157937A"; //35 url
			
	   		//String workingKey = "F6434E2DEDF0D6BDCA01BEC291323463";    // Put in the Working Key provided by CCAVENUES test			
				Integer transactionId = null ;
				try {
					transactionId = Integer.parseInt( URLEncoder.encode(helper.randomfourdigitnumber(),"UTF-8"));
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String orderId = helper.randomfourdigitnumber();
				
				logger.debug("ttrrrf>>>> " + transactionId );
				
	   		 
	   		 StringBuffer ccaRequest = new StringBuffer();
	   		 try {
	   		 ccaRequest.append("tid=").append(transactionId).append("&");
	   		 ccaRequest.append("merchant_id=").append(merchantId).append("&");
	   		 ccaRequest.append("order_id=").append(helper.randomfourdigitnumber()).append("&");
	   		 ccaRequest.append("currency=").append("INR").append("&");
	   		 ccaRequest.append("amount=").append(Price).append("&");
	   		ccaRequest.append("redirect_url=").append("http://35.200.165.138/").append("&");
	   		ccaRequest.append("cancel_url=").append("http://35.200.165.138/").append("&");
	   		ccaRequest.append("language=").append("EN").append("&");
	   		ccaRequest.append("billing_name=").append(first_name).append("&");
	   		//ccaRequest.append("billing_address=").append("Santacruz").append("&");
	   		//ccaRequest.append("billing_city=").append("Mumbai").append("&");
	   		//ccaRequest.append("billing_state=").append("MH").append("&");
	   		//ccaRequest.append("billing_zip=").append("400054").append("&");
	   		//ccaRequest.append("billing_country=").append("India").append("&");
	   		//ccaRequest.append("billing_tel=").append(phone).append("&");
	   		ccaRequest.append("billing_email=").append(email).append("&");
	   		//ccaRequest.append("delivery_name=").append("Sam").append("&");
	   		//ccaRequest.append("delivery_address=").append("Vile Parle").append("&");
	   		//ccaRequest.append("delivery_city=").append("Mumbai").append("&");
	   		//ccaRequest.append("delivery_state=").append("Maharashtra").append("&");
	   		//ccaRequest.append("delivery_zip=").append("400038").append("&");
	   		//ccaRequest.append("delivery_country=").append("India").append("&");
	   		
	   		//ccaRequest.append("delivery_tel=").append("0221234321").append("&");
	   		//ccaRequest.append("merchant_param1=").append("").append("&");
	   		//ccaRequest.append("merchant_param2=").append("").append("&");
	   		//ccaRequest.append("merchant_param3=").append("").append("&");
	   		//ccaRequest.append("merchant_param4=").append("").append("&");
	   		//ccaRequest.append("merchant_param5=").append("").append("&");
	   		//ccaRequest.append("integration_type=").append("").append("&");
	   		//ccaRequest.append("promo_code=").append("").append("&");
	   		ccaRequest.append("customer_identifier=").append("skillmatic");
	   		 } catch (Exception e) {
	   			 logger.debug("ERR:>> " + e.getMessage());
	   		 }
	   		
	   		 logger.debug("CCA Request >> " + ccaRequest.toString());
	   		 
		   	 AesCryptUtil aesUtil=new AesCryptUtil(workingKey);
			 String encRequest = aesUtil.encrypt(ccaRequest.toString());
			 

			 logger.debug("tranasction >>>>> " + courseid.toString());

			 logger.debug("tranasction >>>>> " + orderId);

			 logger.debug("tranasction >>>>> " + usrId);
			 
			 Transaction transaction = new Transaction();
			 
			 transaction.setCourseid(courseid.toString());
			 transaction.setOrderid(orderId);
			 transaction.setUserid(usrId);
			 
			 
			 transaction.setTransactionid(transactionId);
			 transaction.setPrice(Price);
			 
			 logger.debug("tranasction >>>>> " + transaction);
			 
			 
			 transactionService.createTransaction(transaction);
			 
		//	 System.out.println("encrequest >> " + encRequest);
			 //pre testing tid=1527827368758&merchant_id=162062&order_id=1&currency=INR&amount=1.00&
		//	 tid=a852330c54574&merchant_id=162062&order_id=c89c&currency=INR&amount=1&redirect_url=http://35.200.165.138/&cancel_url=http://35.200.165.138/&language=EN&billing_name=Peter&billing_address=Santacruz&billing_city=Mumbai&billing_state=MH&billing_zip=400054&billing_country=India&billing_tel=0229874789&billing_email=testing@domain.com&delivery_name=Sam&delivery_address=Vile Parle&delivery_city=Mumbai&delivery_state=Maharashtra&delivery_zip=400038&delivery_country=India&delivery_tel=0221234321&merchant_param1=none&merchant_param2=none&merchant_param3=none&merchant_param4=none&merchant_param5=none&integration_type=iframe_normal&promo_code=none&customer_identifier=skillmat			 
			 logger.debug("encrequest >> " + encRequest);
	   		
			 return new ResponseEntity<>(new APIResponse("SUCCESS",HttpStatus.OK, encRequest), HttpStatus.OK);	 
				
			}
	
			
	
	}
	
	